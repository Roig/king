/*
 * Exceptions.h
 *
 *  Created on: 03/04/2015
 *      Author: Dani
 */

#ifndef CORE_EXCEPTIONS_H_
#define CORE_EXCEPTIONS_H_

#include <core/Types.h>
#include <exception>
#include <string>


namespace king {

class RTException: public std::exception {
public:

public:
	virtual ~RTException() throw() {}

	explicit RTException(const std::string &a_err_msg = "", const std::string &a_file = "", const u32 a_line = 0, const std::string & a_function = ""):
		msg(format(a_err_msg, a_file, a_line,a_function))
	{
	}

	std::string format(const std::string &a_err_msg, const std::string &a_file, const u32 a_line,const std::string &function )
	{
		return ("+++ Exception +++\nfile: "	+a_file + "\nfunction: " + function + "\nmessage: "+ a_err_msg	);
	}

	const char * what () const throw ()
	{
		return msg.c_str();
	}

private:
	std::string msg;
};

// Just print file/line/function information and exit
#define KING_error() throw king::RTException("",__FILE__,__LINE__,  __PRETTY_FUNCTION__)

// Print file/line/function information plus msg and exit
#define KING_merror(msg) throw king::RTException(msg,__FILE__,__LINE__,  __PRETTY_FUNCTION__)

// Conditionally print file/line/function information and exit
#define KING_assert(test) if(!(test)) throw king::RTException("assertion failed " #test,__FILE__,__LINE__,  __PRETTY_FUNCTION__)

// Conditionally print file/line/function information plus msg and exit
#define KING_massert(test,msg) if(!(test)) throw king::RTException("assertion failed " #test ", " msg,__FILE__,__LINE__,  __PRETTY_FUNCTION__)




}


#endif
