#ifndef CORE_RECT_H_
#define CORE_RECT_H_
#include <core/Vec.h>

namespace king
{
	template <typename T>
	class Rect
    {

    public:
    	explicit Rect() : Rect(0) {};
        explicit Rect(const T scalar) : x(0),y(0),w(scalar),h(scalar) {};
        explicit Rect(const T width, const T height) : x(0),y(0),w(width),h(height) {};
        explicit Rect(const T _x, const T _y,const T width, const T height) : x(_x),y(_y),w(width),h(height) {};
        explicit Rect(const Vec2<T>& sizein) : Rect(sizein.x, sizein.y) {};
        explicit Rect(const Vec2<T>& position, const Vec2<T>& sizein) : Rect(position.x, position.y, sizein.x, sizein.y) {};


        Vec2<T> tl() const
		{
        	 return Vec2<T>(x,y);
		}

        Vec2<T> br() const
        {
        	return Vec2<T>(x+w, y+h);
        }

        Rect<T> size() const
		{
        	return Rect<T>(w, h);
		}

        bool contains(const Vec2<T>& point) const
        {
        	return x <= point.x && point.x < x + w && y <= point.y && point.y < y + h;
        }

        T area() const
        {
        	return w*h;
        }

    public:
        T x,y,w,h;

    };

    typedef Rect<u32> Rectu;
	typedef Rect<s32> Recti;
	typedef Rect<f32> Rectf;
	typedef Rect<f64> Rectd;

}



#endif /* CORE_RECT_H_ */
