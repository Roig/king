#ifndef CORE_MEMORY_H_
#define CORE_MEMORY_H_


#include <memory>

namespace king{

	//helper function
	template<typename T, typename ...Args>
	std::unique_ptr<T> make_unique( Args&& ...args )
	{
		return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
	}


}



#endif /* CORE_MEMORY_H_ */
