#ifndef CORE_CORE_H_
#define CORE_CORE_H_

#include <core/Memory.h>
#include "StateManager.h"
#include "Singleton.h"
#include "input/InputManager.h"

namespace king{


class SoundManager;
class LogManager;
class RenderManager;
class TimeManager;
class Core {
public:
	StateManager& getStateMgr();
	RenderManager& getRenderMgr();
	InputManager& getInputMgr();
	TimeManager& getTimeMgr();
	SoundManager& getSoundMgr();

	void init();
	void run();
	void stop();

	~Core();
private:
	friend SingletonHolder<Core>;
	Core();

	std::unique_ptr<RenderManager> 	m_rendermgr;
	std::unique_ptr<LogManager> 	m_logMgr;
	std::unique_ptr<TimeManager>	m_timemgr;
	std::unique_ptr<SoundManager>	m_soundmgr;
	StateManager					m_statemgr;
	InputManager					m_inputmgr;
	bool m_running;
};


typedef SingletonHolder<Core> CoreSingleton;

}
#endif /* CORE_CORE_H_ */
