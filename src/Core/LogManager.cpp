#include <core/LogManager.h>
#include <chrono>
#include <iostream>

namespace king {

LogManager::LogManager()
{
	m_outfilestream.open("kinglog.txt");
	if (!m_outfilestream.is_open())
	{
		std::cout<< "ERROR opening Log file";
	}

}

LogManager::~LogManager()
{
	if (m_outfilestream.is_open())
		m_outfilestream.close();
}

void LogManager::log(const std::string& msg, const LOG_LEVEL ll)
{
	if (m_outfilestream.is_open())
	{
		auto now = std::chrono::system_clock::now();
		auto now_time_t = std::chrono::system_clock::to_time_t( now );
		auto now_tm = std::localtime( &now_time_t );

		std::string levelstr;
		switch(ll)
		{
		case LL_INFO:
			levelstr="INFO";
			break;
		case LL_WARN:
			levelstr="WARNING";
			break;
		case LL_ERR:
			levelstr="ERROR";
			break;
		default:
			levelstr="UNKNOWN";
			break;

		}

		m_outfilestream<<"["<<levelstr<<"]["<< now_tm->tm_hour << ":" <<
				now_tm->tm_min << ":" << now_tm->tm_sec << "]: " << msg << std::endl;

	}
}

} /* namespace king */
