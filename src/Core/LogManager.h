#ifndef CORE_LOGMANAGER_H_
#define CORE_LOGMANAGER_H_

#include <fstream>

namespace king {

class LogManager {
public:

	LogManager();
	~LogManager();

	typedef enum
	{
		LL_INFO = 0,
		LL_WARN,
		LL_ERR
	}LOG_LEVEL;

	void log(const std::string &msg, const LOG_LEVEL ll = LL_INFO);
private:
	std::ofstream m_outfilestream;
};

} /* namespace king */

#endif /* CORE_LOGMANAGER_H_ */
