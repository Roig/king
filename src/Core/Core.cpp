#include <core/Core.h>
#include <core/LogManager.h>
#include <SDL2/SDL.h>
#include <iostream>
#include "render/RenderManager.h"
#include "input/InputManager.h"
#include <time/TimeManager.h>
#include <sound/SoundManager.h>

namespace king {





Core::Core():
		m_running(false)
{
}

Core::~Core() {

	m_logMgr.release();

	SDL_Quit();
}

void Core::init()
{
	SDL_Init( SDL_INIT_VIDEO );

	m_logMgr = make_unique<LogManager>();
	m_rendermgr = make_unique<RenderManager>();

	m_timemgr = make_unique<TimeManager>();
	m_soundmgr = make_unique<SoundManager>();
	m_rendermgr->init("King", 755, 600, false);
	m_soundmgr->init();
	m_running = true;
}

void Core::run()
{
	while (m_running)
	{
		m_timemgr->update();
		m_inputmgr.update();
		//std::cout<<m_timemgr->getDT()<< " " << m_timemgr->getFPS()<<std::endl;
		m_statemgr.top()->update(m_timemgr->getDT());
		m_rendermgr->render();
	}
}

void Core::stop()
{
	m_running = false;
}

StateManager& Core::getStateMgr() {
	return m_statemgr;
}

RenderManager& Core::getRenderMgr() {
	return *m_rendermgr.get();
}
InputManager& Core::getInputMgr() {
	return m_inputmgr;
}
SoundManager& Core::getSoundMgr() {
	return *m_soundmgr.get();
}
}

