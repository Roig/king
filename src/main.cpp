

#include <core/Core.h>
#include "game/states/MainMenuState.h"
#include "game/states/MainGameState.h"

int main( int argc, char* args[] )
{

	king::CoreSingleton::get().init();

	auto& statemgr = king::CoreSingleton::get().getStateMgr();
	statemgr.registerState(MainMenuStateSingleton::get_ptr());
	statemgr.registerState(MainGameStateSingleton::get_ptr());

	statemgr.changeState(MainMenuStateSingleton::get_ptr());

	king::CoreSingleton::get().run();

	return 0;

}
