#ifndef CORE_STATEMANAGER_H_
#define CORE_STATEMANAGER_H_

#include "State.h"
#include <vector>
#include <stack>
#include <unordered_map>
#include <string>

namespace king {


class StateManager
{
public:
	StateManager(const std::string& a_Name = "UNNAMED");
	~StateManager();

	void registerState(State* state);
	void unregisterState(State* state);
	void changeState(State* state);
	void pushState(State* state);
	void pop();
	void clear();

	State* top();
	std::string getName() const;

	std::vector<State *> getAllRegisteredStates() const;
	void changeState(const std::string& a_StateName);
	void pushState(const std::string& a_StateName);
	State* getState(const std::string& a_StateName);

protected:
	typedef std::stack<State*>	t_stateStack;
	typedef std::unordered_map<std::string, State*>	t_registeredStatesMap;

	t_stateStack				m_StateStack;
	t_registeredStatesMap		m_RegisteredStates;
	std::string					m_name;
};

} /* namespace king */

#endif /* CORE_STATEMANAGER_H_ */
