
#include <core/StateManager.h>

namespace king {

StateManager::StateManager(const std::string& a_Name) :
	m_name{a_Name}
{

}

StateManager::~StateManager()
{
	clear();
}

void StateManager::registerState(State* a_State)
{
	if (a_State)
	{
		auto it = m_RegisteredStates.find(a_State->getName());

		if (it == m_RegisteredStates.end())
		{
			m_RegisteredStates.emplace_hint(it,	t_registeredStatesMap::value_type(a_State->getName(), a_State));
		}
		else
		{
			throw std::runtime_error("attempt to register a duplicated state name in state manager " + getName());
		}
	}
}

void StateManager::unregisterState(State* a_State)
{
	if (a_State)
	{
		//LOG(LOG_DEVEL, LOGSUB_ENGINE, "[%s] UnregisterState %s", getName().c_str(),a_State->getName().c_str());

		m_RegisteredStates.erase(a_State->getName());
	}
}

State* StateManager::getState(const std::string& a_StateName)
{
	State* state = nullptr;
	auto it = m_RegisteredStates.find(a_StateName);

	if (it != m_RegisteredStates.end())
	{
		state = it->second;
	}

	return state;
}

void StateManager::changeState(State* a_State)
{
	if(a_State)
	{
		// #todo: parlar. Faig que un change state desapili i cridi al Finish de tots els estats apilats.
		std::string originStatesStr;

		while (State* originState = m_StateStack.empty() ? nullptr : m_StateStack.top())
		{
			// Exit state and pop it from stack
			originState->finish();
			originStatesStr += originState->getName() + " ";

			m_StateStack.pop();
		}

		// Store and init the new state.
		m_StateStack.push(a_State);
		m_StateStack.top()->start();

		/*LOG( LOG_DEVEL, LOGSUB_STATE ,"[%s] ChangeState %s -> %s",
				getName().c_str(),
			originStatesStr.empty() ? "-" : originStatesStr.c_str(),
			a_State->getName().c_str());*/
	}
}

void StateManager::changeState(const std::string& a_StateName)
{
	auto it = m_RegisteredStates.find(a_StateName);

	if (it != m_RegisteredStates.end())
	{
		changeState(it->second);
	}
}

void StateManager::pushState(State* a_State)
{
	if(a_State)
	{
		State* originState = m_StateStack.empty()? nullptr : m_StateStack.top();
		if (originState)
		{
			// Pause current state
			originState->pause();
		}

		// Store and init the new state.
		m_StateStack.push(a_State);
		m_StateStack.top()->start();

	/*	LOG( LOG_DEVEL, LOGSUB_STATE ,"[%s] PushState %s on top of %s",
			m_Name.c_str(),
			a_State->GetName().c_str(),
			originState? originState->getName().c_str():"-");*/
	}
}

void StateManager::pushState(const std::string& a_StateName)
{
	auto it = m_RegisteredStates.find(a_StateName);

	if (it != m_RegisteredStates.end())
	{
		pushState(it->second);
	}
}

void StateManager::pop()
{
	State* originState = m_StateStack.empty()? nullptr : m_StateStack.top();
	if (originState)
	{
		// Finish current state and pop it
		originState->finish();
		m_StateStack.pop();
	}

	State* destState = m_StateStack.empty()? nullptr : m_StateStack.top();
	if (destState)
	{
		// Resume the last state
		destState->resume();
	}

	/*LOG( LOG_DEVEL, LOGSUB_STATE ,"[%s] PopState %s from %s",
		m_Name.c_str(),
		originState? originState->getName().c_str():"-",
		destState? destState->getName().c_str():"-");*/
}

void StateManager::clear()
{
	while (!m_StateStack.empty())
	{
		m_StateStack.top()->finish();
		m_StateStack.pop();

	//	LOG(LOG_DEVEL, LOGSUB_STATE, "[%s] Stack cleared", getName().c_str());
	}
}

State* StateManager::top()
{
	State* state = nullptr;

	if (!m_StateStack.empty())
	{
		state = m_StateStack.top();
	}

	return state;
}

std::string StateManager::getName() const
{
	return m_name;
}

std::vector<State *> StateManager::getAllRegisteredStates() const
{
	std::vector<State *> ret_vec;
	for (auto &state : m_RegisteredStates)
		ret_vec.push_back(state.second);

	return ret_vec;

}

}

