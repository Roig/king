#ifndef CORE_VEC2_H_
#define CORE_VEC2_H_



#include <core/Types.h>
#include <cmath>

namespace king{

	template<typename T>
	class Vec2
	{
	public:
		union { T x; T u; T w;};
		union { T y; T v; T h;};

	public:

		explicit Vec2() : Vec2(0) {};
		explicit Vec2(const T &scalar) : x(scalar), y(scalar) {}
		explicit Vec2(const T &p1, const T &p2) : x(p1), y(p2) {}


		Vec2<T>& operator=(const Vec2<T>& v) {
			x = v.x;
			y = v.y;
			return *this;
		}

		Vec2<T> operator+(const Vec2<T>& v) const
		{
			return Vec2<T>(x + v.x, y + v.y);
		}

		Vec2<T> operator-(const Vec2<T>& v) const
		{
			return Vec2<T>(x - v.x, y - v.y);
		}

		bool operator==( const Vec2<T>& rhs) const
		{
			return ((this->x == rhs.x) && (this->y == rhs.y));
		}

		bool operator!=(const Vec2<T>& rhs) const
		{
			return !operator==(rhs);
		}

		inline bool operator < ( const  Vec2<T>& rhs ) const
		{
			if( x < rhs.x && y < rhs.y )
				return true;
			return false;
		}


		Vec2<T>& operator+=(Vec2<T>& v)
		{
			x += v.x;
			y += v.y;
			return *this;
		}

		Vec2<T>& operator-=(Vec2<T>& v)
		{
			x -= v.x;
			y -= v.y;
			return *this;
		}

		f32 normalise()
		{
			f32 len = length();

			if ( len > (0.0f) )
			{
				f32 p = 1.0f / len;
				x *= p;
				y *= p;
			}

			return len;
		}


		f32 length() const
		{
			return std::sqrt(x * x + y * y);
		}

		f32 dist(Vec2<T> v) const
		{
			Vec2<T> d(v.x - x, v.y - y);
			return d.length();
		}

	};

	typedef Vec2<u8> Vec2b;
    typedef Vec2<u32> Vec2u;
	typedef Vec2<s32> Vec2i;
	typedef Vec2<f32> Vec2f;
	typedef Vec2<f64> Vec2d;
	
	
	template<typename T>
	class Vec3
	{
	public:
		union { T x; T r;};
		union { T y; T g;};
		union { T z; T b;};
		

	public:
		Vec3() : Vec3(0) {};
		explicit Vec3(const T &scalar) : x(scalar), y(scalar), z(scalar) {}
		explicit Vec3(const T &p1, const T &p2, const T &p3) : x(p1), y(p2), z(p3) {}
	};

	typedef Vec3<u8> Vec3b;
    typedef Vec3<u32> Vec3u;
	typedef Vec3<s32> Vec3i;
	typedef Vec3<f32> Vec3f;
	typedef Vec3<f64> Vec3d;
	
	
	template<typename T>
	class Vec4
	{
	public:
		union { T x; T r;};
		union { T y; T g;};
		union { T z; T b;};
		union { T w; T a;};

	public:
		Vec4() : Vec4(0) {};
		explicit Vec4(const T &scalar) : x(scalar), y(scalar), z(scalar), w(scalar) {}
		explicit Vec4(const T &p1, const T &p2, const T &p3, const T &p4) : x(p1), y(p2), z(p3), w(p4) {}

		bool operator==( const Vec4<T>& rhs) const
		{
			return ((this->x == rhs.x) && (this->y == rhs.y)  && (this->z == rhs.z) && (this->w == rhs.w));
		}

		bool operator!=(const Vec4<T>& rhs) const
		{
			return !operator==(rhs);
		}


	};

	typedef Vec4<u8> Vec4b;
    typedef Vec4<u32> Vec4u;
	typedef Vec4<s32> Vec4i;
	typedef Vec4<f32> Vec4f;
	typedef Vec4<f64> Vec4d;
	


} /* namespace king */

#endif /* CORE_VEC_H_ */
