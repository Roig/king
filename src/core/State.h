#ifndef CORE_STATE_H_
#define CORE_STATE_H_

#include "Types.h"
#include <string>

namespace king {

class State
{
public:
	State(const std::string& name = "UNDEFINEDSTATE"): m_name(name){};
	virtual ~State(){};
	virtual void start() {};
	virtual void finish() {};
	virtual void pause() {};
	virtual void resume() {};
	virtual void update(f64 dt){};
	const std::string& getName() const {return m_name; }

private:
	std::string m_name;
};





} /* namespace king */

#endif
