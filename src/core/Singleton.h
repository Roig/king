#ifndef CORE_SINGLETON_H_
#define CORE_SINGLETON_H_

namespace king {

template <typename T>
class SingletonHolder
{
public:
	typedef T ObjectType;
	static T& get();
	static T* get_ptr();

private:
	SingletonHolder();
	static T *m_instance;
};


template <class T>
typename SingletonHolder<T>::ObjectType* SingletonHolder<T>::m_instance;


template < class T>
inline T& SingletonHolder<T>::get()
{
	if (!m_instance)
		m_instance = new T;

	return *m_instance;
}

template < class T>
inline T* SingletonHolder<T>::get_ptr()
{
	if (!m_instance)
		m_instance = new T;

	return m_instance;
}


}


#endif /* CORE_SINGLETON_H_ */
