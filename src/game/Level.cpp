#include <game/Level.h>
#include <core/Exceptions.h>
#include <core/Core.h>
#include <input/InputManager.h>
#include <iostream>
#include <algorithm>
#include <set>
#include <chrono>
#include <sound/SoundManager.h>

//SOUNDS
const std::string SOUND_GEMDESTROYED("media/gemdestroy.ogg");
const std::string SOUND_HYPERGEMDESTROYED("media/hypergemdestroy.ogg");

const std::string SOUND_PRIZE1("media/1.ogg");
const std::string SOUND_PRIZE2("media/2.ogg");
const std::string SOUND_PRIZE3("media/3.ogg");
const std::string SOUND_PRIZE4("media/4.ogg");
const std::string SOUND_PRIZE5("media/5.ogg");

//FONTS
const std::string FONT_DEFAULT("media/OrbitronBlack.ttf");

//BACKGROUND SCORE
const std::string SCOREBACK_FILEPATH("media/timezone.png");
const king::Vec2f SCOREBACK_POS(56,84);
const u32 SCOREBACK_Z(51);



//BACKGROUND FINAL SCORE
const king::Vec2f SCOREENDBACK_POS(400,280);
const u32 SCOREENDBACK_Z(20);
const f64 SCOREENDFINAL_TIME_SEC(10.0);
const king::Vec2f SCOREENDANCHOR_POS(0.5,0.5);

const king::Vec2f SCOREENDBACK_TEXT1POS(390,170);
const king::Vec2f SCOREENDBACK_TEXT2POS(390,280);
const u32 SCOREENDBACK_TEXTZ(SCOREENDBACK_Z - 1);
const f64 SCOREENDFINALBLINK_TIME_SEC(1.0);
const king::color4b::color4b SCOREENDBLINK_COLOR1(251,234,0,255);
const king::color4b::color4b SCOREENDBLINK_COLOR2(251,75,0,255);

//TAPA TOKENS
const std::string TAPA_FILEPATH("media/tapa.png");
const king::Vec2f TAPA_POS(329,34);
const u32 TAPA_Z(40);

//TOKENS TABLE
const u32 ROWS = 8;
const u32 COLS = 8;
const king::Vec2f START_POS(355,125);
const king::Vec2f TOKEN_OFFSET(5,5);
const u32 TOKEN_Z(50);

//BACKGROUND
const std::string BACKGROUND_FILEPATH("media/BackGround.jpg");
const u32 BACKGROUND_Z(100);


//TIMER
const f64 TIME_LIMIT_SEC(60);
const king::Vec2f TIMER_TEXT_POS(70,180);
const king::Vec2f TIMER_TEXTNUM_POS(85,215);
const u32 TIMER_Z(50);
const king::color4b::color4b TIMER_COLOR(251,234,0,255);

//SCORE



const f32 REFILL_DELAY(0.25);
const f32 GRAB_TRESHOLD(0.25);


Level::Level(): m_mt(std::chrono::high_resolution_clock::now().time_since_epoch().count()), m_state(INIT)
{
	m_grabtimer = GRAB_TRESHOLD;
	m_ended = false;
	m_timeremaining = TIME_LIMIT_SEC;
	m_prizesshowed=0;
	m_backscore = king::Sprite::create(SCOREBACK_FILEPATH);
	m_backscore->setPos(SCOREBACK_POS);
	m_backscore->setDepthZ(SCOREBACK_Z);
	m_backscore->setVisible(false);

	m_tapa = king::Sprite::create(TAPA_FILEPATH);
	m_tapa->setPos(TAPA_POS);
	m_tapa->setDepthZ(TAPA_Z);
	m_tapa->setVisible(false);

	m_background = king::Sprite::create(BACKGROUND_FILEPATH);
	m_background->setDepthZ(BACKGROUND_Z);
	m_background->setVisible(false);

	m_timenumbertext = king::Text::create(FONT_DEFAULT, 24);
	m_timenumbertext->setDepthZ(TIMER_Z);
	m_timenumbertext->setColor(TIMER_COLOR);
	m_timenumbertext->setPos(TIMER_TEXTNUM_POS);
	m_timenumbertext->setText(getTimeRemainingText());
	m_timenumbertext->setVisible(false);

	m_timetext = king::Text::create(FONT_DEFAULT, 32);
	m_timetext->setDepthZ(TIMER_Z);
	m_timetext->setColor(TIMER_COLOR);
	m_timetext->setPos(TIMER_TEXT_POS);
	m_timetext->setText("TIME");
	m_timetext->setVisible(false);




	m_endbackground = king::Sprite::create(SCOREBACK_FILEPATH);
	m_endbackground->setPos(SCOREENDBACK_POS);
	m_endbackground->setDepthZ(SCOREENDBACK_Z);
	m_endbackground->setScale(6,6);
	m_endbackground->setAnchor(SCOREENDANCHOR_POS);
	m_endbackground->setVisible(false);

	m_finalscoretext = king::Text::create(FONT_DEFAULT, 72);
	m_finalscoretext->setDepthZ(SCOREENDBACK_TEXTZ);
	m_finalscoretext->setColor(SCOREENDBLINK_COLOR1);
	m_finalscoretext->setPos(SCOREENDBACK_TEXT1POS);
	m_finalscoretext->setAnchor(SCOREENDANCHOR_POS);
	m_finalscoretext->setText("SCORE");
	m_finalscoretext->setVisible(false);

	m_finalscorenum = king::Text::create(FONT_DEFAULT, 64);
	m_finalscorenum->setDepthZ(SCOREENDBACK_TEXTZ);
	m_finalscorenum->setColor(SCOREENDBLINK_COLOR1);
	m_finalscorenum->setPos(SCOREENDBACK_TEXT2POS);
	m_finalscorenum->setAnchor(SCOREENDANCHOR_POS);
	m_finalscorenum->setText("NUMBER");
	m_finalscorenum->setVisible(false);


}

void Level::start()
{
	m_score.reset();
	m_ended = false;
	setVisible(true);

	m_timeremaining = TIME_LIMIT_SEC;

	generateTokenTableWithoutPrizes();
	m_state = USER_INPUT_SELECTION;
}


void Level::generateTokenTableWithoutPrizes()
{
	king::Vec2f curr_pos(START_POS);


	for (auto i = 0u; i < ROWS; i++)
	{
		curr_pos.y = START_POS.y;
		for (auto j = 0u; j < COLS; j++)
		{
			m_tokentable[i][j] = king::make_unique<Token>( getTokenTypeNoPrize(king::Vec2i(i,j)), king::Vec2f(curr_pos), TOKEN_Z );
			m_positiontable[i][j] = curr_pos;

			curr_pos.y += TOKEN_OFFSET.y + m_tokentable[i][j]->getCollisionArea().h;
		}
		curr_pos.x += TOKEN_OFFSET.x + m_tokentable[0][0]->getCollisionArea().w;
	}

	for (auto col = 0u; col < m_starttokenpos.size(); col++)
	{
		king::Vec2f pos = m_tokentable[col][0]->getPos();
		pos.y -= (TOKEN_OFFSET.y + m_tokentable[col][0]->getCollisionArea().h);
		m_starttokenpos[col] = pos;
	}

}


::Token::TokenType Level::getTokenTypeNoPrize(const king::Vec2i& position)
{
	std::set<Token::TokenType> candidates = {Token::BLUE, Token::GREEN, Token::PURPLE,Token::RED, Token::YELLOW};


	//Vertical
	if (position.y >= 2)
		if ( m_tokentable[position.x][position.y - 1]->getType() ==  m_tokentable[position.x][position.y - 2]->getType())
			candidates.erase(m_tokentable[position.x][position.y - 1]->getType());

	//Horitzontal
	if (position.x >= 2)
		if ( m_tokentable[position.x - 1][position.y]->getType() ==  m_tokentable[position.x- 2][position.y]->getType())
			candidates.erase(m_tokentable[position.x - 1][position.y]->getType());

	Token::TokenType candidate;
	if (candidates.size())
	{
		std::uniform_int_distribution<u32> dist(0, candidates.size() - 1);
		candidate = *std::next(candidates.begin(), dist(m_mt));
	}
	else
		KING_merror("No candidates found!!");

	return candidate;
}


Level::~Level()
{


}

void Level::update(f64 dt)
{



	m_timeremaining -= dt;
	if (m_timeremaining < 0)
		m_timeremaining = 0.0;

	m_timenumbertext->setText(getTimeRemainingText());


	for (auto &vectoken: m_tokentable)
		for (auto &token: vectoken)
			token->update(dt);




	switch(m_state)
	{
	default:
		break;
	case USER_INPUT_SELECTION:

		updateUserInput(dt);
		break;
	case USER_INPUT_GRAB:
		updateUserGrab(dt);
		break;
	case USER_INPUT_SELECTED:
		updateUserInputSelected(dt);
		break;
	case ANIMATE_SELECTION_FORWARD:
		updateAnimationSelectedForward(dt);
		break;
	case ANIMATE_SELECTION_BACK:
		updateAnimationSelectedBack(dt);
		break;
	case ANIMATE_PRIZES:
		updateAnimatePrizes(dt);
		break;

	case ANIMATE_NEWTOKENS:
		updateAnimateNewTokens(dt);
		break;
	case END:
		updateEnd(dt);
		break;
	}

	m_score.udpate(dt);
}

king::Vec2i Level::getTokenIndexByPositionExclude(const king::Vec2f& pos, bool &found, const king::Vec2i& excludeindex)
{
	king::Vec2i index;
	found = false;
	for (auto i = 0; i < COLS; i++)
	{
		for (auto j = 0; j < ROWS; j++)
		{
			if (!((excludeindex.x == i ) && (excludeindex.y == j )))
			{
				auto &token = m_tokentable[i][j];
				auto recttoken = token->getCollisionArea();
				if (recttoken.contains(pos) )
				{
					found = true;
					index.x = i;
					index.y = j;
					return index;
				}
			}
		}

	}
	return index;
}

void Level::updateUserGrab(f64 dt)
{
	auto &core = king::CoreSingleton::get();
	auto &mouse = core.getInputMgr().getMouse();
	auto mousepos = mouse.getPos<king::Vec2f>();

	//Player releases button
	if(!mouse.isButtonPushed(king::Mouse::LEFT))
	{
		auto &tok = m_tokentable[m_selected[0].x][m_selected[0].y];
		bool isatoken = false;
		auto tokenidx = getTokenIndexByPositionExclude(mousepos, isatoken, m_selected[0]);
		bool isadjacent = false;
		if (isatoken)
		{
			isadjacent = isTokenAdjacent(m_selected[0], tokenidx);
		}

		if (isadjacent)
		{
			auto &token = m_tokentable[tokenidx.x][tokenidx.y];
			token->setState(Token::SELECTED);
			m_selected[1] = tokenidx;
			swapTokens(m_selected[0], m_selected[1]);
			m_state = ANIMATE_SELECTION_FORWARD;
		}
		else
		{
			//CAS QUE NO HI HAGI PREMI
			tok->setState(Token::IDLE);
			tok->setPos(m_positiontable[m_selected[0].x][m_selected[0].y]);
			m_state = USER_INPUT_SELECTION;
		}

	}
	else
	{
		for (auto i = 0u; i < m_tokentable.size(); i++)
		{
			for (auto j = 0u; j < m_tokentable[i].size(); j++)
			{

				auto position = king::Vec2i(i,j);
				if ((m_selected[0] != position ) && isTokenAdjacent(m_selected[0], position))
				{
					auto &token = m_tokentable[i][j];
					auto recttoken = token->getCollisionArea();

					if (recttoken.contains(mousepos) )
						token->setState(Token::HOVER);
					else
						token->setState(Token::IDLE);
				}
			}
		}
	}
}


void Level::updateEnd(f64 dt)
{
	static f64 m_endtimer = SCOREENDFINAL_TIME_SEC;
	static f64 m_timerblink = SCOREENDFINALBLINK_TIME_SEC;

	if (!m_timerblink)
	{
		if (m_finalscorenum->getColor() == SCOREENDBLINK_COLOR1)
			m_finalscorenum->setColor(SCOREENDBLINK_COLOR2);
		else
			m_finalscorenum->setColor(SCOREENDBLINK_COLOR1);
		m_timerblink = SCOREENDFINALBLINK_TIME_SEC;
	}
	else
	{
		m_timerblink -= dt;
		if (m_timerblink < 0)
			m_timerblink = 0.0;
	}


	if (m_endtimer)
	{
		m_finalscorenum->setText(std::to_string(m_score.getTotalScore()));
		m_finalscorenum->setVisible(true);
		m_finalscoretext->setVisible(true);
		m_endbackground->setVisible(true);

		m_timetext->setVisible(false);
		m_timenumbertext->setVisible(false);
		m_backscore->setVisible(false);
		m_score.setVisible(false);

		m_endtimer -= dt;
		if (m_endtimer<0)
			m_endtimer =0.0;
	}
	else
	{
		m_finalscorenum->setVisible(false);
		m_finalscoretext->setVisible(false);
		m_endbackground->setVisible(false);

		m_endtimer = SCOREENDFINAL_TIME_SEC;
		m_ended = true;
	}

}

void Level::setVisible(const bool visible)
{
	m_backscore->setVisible(visible);
	m_tapa->setVisible(visible);
	m_background->setVisible(visible);
	m_timenumbertext->setVisible(visible);
	m_timetext->setVisible(visible);
	m_score.setVisible(visible);


	for (auto &p: m_tokentable)
	{
		for (auto &token: p)
		{
			if (token)
				token->setVisible(visible);
		}
	}

}

void Level::updateAnimateNewTokens(f64 dt)
{
	bool ended = true;
	for (auto &p: m_tokentable)
	{
		for (auto &token: p)
		{
			if ( token->getState() != Token::IDLE)
				ended = false;
		}
	}

	if (ended)
	{

		std::vector<king::Vec2i> positions;
		for(auto i = 0; i < ROWS; i++)
			for(auto j = 0; j < COLS; j++)
				positions.push_back(king::Vec2i(i,j));

		m_currentprizes = getNewPrizes(positions);

		if (m_currentprizes.size())
		{
			m_state = ANIMATE_PRIZES;
		}
		else
		{
			m_score.addPartialToTotal();
			m_prizesshowed = 0;
			m_state = USER_INPUT_SELECTION;
		}


	}

}

void Level::swapColumn(s32 col)
{

	king::Vec2i emptypos(0);
	bool emptyfound = false;

	king::Vec2i tokenpos(0);
	bool tokenfound = false;

	for (king::Vec2i pos1(col, ROWS - 1); (pos1.y >= 0) && !emptyfound; pos1.y--)
	{
		auto &token = m_tokentable[pos1.x][pos1.y];

		if (token->getState() == Token::END)
		{
			emptypos.x = pos1.x;
			emptypos.y = pos1.y;
			emptyfound = true;

			for (king::Vec2i pesa(emptypos.x, emptypos.y - 1); (pesa.y >= 0) && !tokenfound; pesa.y-- )
			{
				auto &tokenpesa = m_tokentable[pesa.x][pesa.y];
				if (tokenpesa->getState() != Token::END)
				{
					tokenpos.x = pesa.x;
					tokenpos.y = pesa.y;
					tokenfound = true;

				}
			}

		}
	}

	if (tokenfound)
	{

		swapTokens(emptypos, tokenpos);
		swapColumn(col);
	}

}

bool Level::allPrizeAnimationsFinished()
{
	for (auto &prize : m_currentprizes)
	{
		if (!prize.isFinished())
			return false;
	}

	return true;
}

bool Level::isAPrizeAnimating()
{
	for (auto &prize : m_currentprizes)
	{
		if (prize.isAnimated())
		{
			if (!prize.isFinished())
				return true;
		}
	}

	return false;
}

void Level::playNextPrizeAnimation()
{
	for (auto &prize : m_currentprizes)
	{
		if (!prize.isAnimated())
		{
			prize.animate();
			playNextPrizeSound(m_prizesshowed);
			m_score.addPartialScore( prize.getScore());

			m_prizesshowed++;
			return;
		}

	}
}

void Level::playNextPrizeSound(const u32 prizecount)
{
	switch(prizecount)
	{
	case 0:
		king::CoreSingleton::get().getSoundMgr().playSound(SOUND_PRIZE1);
		break;
	case 1:
		king::CoreSingleton::get().getSoundMgr().playSound(SOUND_PRIZE2);
		break;
	case 2:
		king::CoreSingleton::get().getSoundMgr().playSound(SOUND_PRIZE3);
		break;
	case 3:
		king::CoreSingleton::get().getSoundMgr().playSound(SOUND_PRIZE4);
		break;

	default:
		king::CoreSingleton::get().getSoundMgr().playSound(SOUND_PRIZE5);
		break;

	}
}

void Level::updateAnimatePrizes(f64 dt)
{
	if (!isAPrizeAnimating())
	{
		playNextPrizeAnimation();
	}

	if (allPrizeAnimationsFinished())
	{
		//Columns to check/swap
		std::set<s32> columnstoswap;
		for (auto &p: m_currentprizes)
			for (auto &pos: p.getIndexs())
				columnstoswap.insert(pos.first.x);

		for (auto it = columnstoswap.begin(); it != columnstoswap.end(); it++)
			swapColumn(*it);


		//new elements
		refillTokenTable();
		m_state = ANIMATE_NEWTOKENS;

	}

}

void Level::refillTokenTable()
{
	std::vector< std::vector<king::Vec2i >> refillpositions;

	for (u32 i = 0; i < m_tokentable.size(); i++)
	{
		std::vector<king::Vec2i> positions;

		for (u32 j = 0; j < m_tokentable[i].size(); j++)
		{
			auto &token = m_tokentable[i][j];
			if (token->getState() == Token::END)
				positions.push_back(king::Vec2i(i,j));
		}

		refillpositions.push_back(positions);
	}


	for (auto &vec: refillpositions )
	{
		if (!vec.empty())
		{
			f32 delay =  REFILL_DELAY;
			for ( auto rit = vec.rbegin(); rit!= vec.rend(); ++rit)
			{
				auto pos = *rit;
				m_tokentable[pos.x][pos.y] = king::make_unique<Token>( generateTokenType(), king::Vec2f(m_starttokenpos[pos.x]), TOKEN_Z );
				auto &tok = m_tokentable[pos.x][pos.y];
				tok->setPosEnd(m_positiontable[pos.x][pos.y]);
				tok->setDelayAnimation(delay);
				tok->setEndSound(true);
				tok->setState(Token::MOVING);
				delay += REFILL_DELAY;
			}
		}
	}

}


void Level::swapTokens(const king::Vec2i& pos1, const king::Vec2i& pos2, const bool endsound )
{
	auto &tok1 = m_tokentable[pos1.x][pos1.y];
	auto &tok2 = m_tokentable[pos2.x][pos2.y];
	tok1.swap(tok2);


	if (tok1->getState() != Token::END)
	{
		tok1->setPosEnd(m_positiontable[pos1.x][pos1.y]);
		tok1->setState(Token::MOVING);
		tok1->setEndSound(endsound);
	}

	if (tok2->getState() != Token::END)
	{
		tok2->setPosEnd(m_positiontable[pos2.x][pos2.y]);
		tok2->setState(Token::MOVING);
		tok1->setEndSound(endsound);
	}

}

void Level::updateAnimationSelectedBack(f64 dt)
{
	auto &tok1 = m_tokentable[m_selected[0].x][m_selected[0].y];
	auto &tok2 = m_tokentable[m_selected[1].x][m_selected[1].y];
	if (tok1->getState() == Token::IDLE && tok2->getState() == Token::IDLE)
		m_state = USER_INPUT_SELECTION;

}

void Level::updateAnimationSelectedForward(f64 dt)
{
	auto &tok1 = m_tokentable[m_selected[0].x][m_selected[0].y];
	auto &tok2 = m_tokentable[m_selected[1].x][m_selected[1].y];

	if (tok1->getState() == Token::IDLE && tok2->getState() == Token::IDLE)
	{
		std::vector<king::Vec2i> vecpos;
		vecpos.push_back(m_selected[0]);
		vecpos.push_back(m_selected[1]);

		m_currentprizes = getNewPrizes(vecpos);


		if (m_currentprizes.size())
		{
			m_state = ANIMATE_PRIZES;
		}
		else
		{
			swapTokens(m_selected[0], m_selected[1]);
			m_state = ANIMATE_SELECTION_BACK;
		}

	}
}
bool Level::checkGrab(f64 dt)
{
	auto &core = king::CoreSingleton::get();
	auto &mouse = core.getInputMgr().getMouse();
	auto endpos = mouse.getPos<king::Vec2i>();
	auto len = king::Vec2i(endpos - m_grabstartpos).length();


	m_grabtimer -= dt;
	if (m_grabtimer < 0.0)
		m_grabtimer = 0.0;

	//std::cout<<"timer: "<< (m_grabtimer)<< " len:  "<<len<<"("<<endpos.x<<","<<endpos.y<<")"<<" "<<"("<<m_grabstartpos.x<<","<<m_grabstartpos.y<<")"<<std::endl;

	if(mouse.isButtonPushed(king::Mouse::LEFT) && ( len >= 15) && m_grabtimer)
	{
	//	std::cout<<"GRABING"<<std::endl;
		m_tokentable[m_selected[0].x][m_selected[0].y]->setState(Token::GRAB);
		m_state = USER_INPUT_GRAB;
		return true;
	}
	return false;
}

void Level::updateUserInputSelected(f64 dt)
{
	auto &core = king::CoreSingleton::get();
	auto &mouse = core.getInputMgr().getMouse();
	auto mousepos = mouse.getPos<king::Vec2f>();

	if(mouse.isButtonKeyDown(king::Mouse::RIGHT) )
	{
		m_tokentable[m_selected[0].x][m_selected[0].y]->setState(Token::IDLE);
		m_state = USER_INPUT_SELECTION;
	}

	if ( !checkGrab(dt) )
	{
		for (auto i = 0u; i < m_tokentable.size(); i++)
		{
			for (auto j = 0u; j < m_tokentable[i].size(); j++)
			{

				auto position = king::Vec2i(i,j);
				if ((m_selected[0] != position ) && isTokenAdjacent(m_selected[0], position))
				{
					auto &token = m_tokentable[i][j];
					auto recttoken = token->getCollisionArea();

					if (recttoken.contains(mousepos) )
					{
						if(mouse.isButtonKeyDown(king::Mouse::LEFT) )
						{
							token->setState(Token::SELECTED);
							m_selected[1] = king::Vec2i(i,j);
							swapTokens(m_selected[0], m_selected[1]);
							m_state = ANIMATE_SELECTION_FORWARD;
						}
						else
							token->setState(Token::HOVER);
					}
					else
						token->setState(Token::IDLE);
				}
			}

		}
	}

}

bool Level::isTokenAdjacent(const king::Vec2i& pos1, const king::Vec2i& pos2)
{
	return (pos1.dist(pos2) == 1);
}

void Level::updateUserInput(f64 dt)
{

	if (m_timeremaining)
	{
		auto &core = king::CoreSingleton::get();
		auto &mouse = core.getInputMgr().getMouse();
		auto mousepos = mouse.getPos<king::Vec2f>();

		for (auto i = 0u; i < m_tokentable.size(); i++)
		{
			for (auto j = 0u; j < m_tokentable[i].size(); j++)
			{
				auto &token = m_tokentable[i][j];
				auto recttoken = token->getCollisionArea();

				if (recttoken.contains(mousepos) )
				{
					if(mouse.isButtonKeyDown(king::Mouse::LEFT) )
					{
						token->setState(Token::SELECTED);
						m_selected[0]=king::Vec2i(i,j);
						m_grabstartpos = mouse.getPos<king::Vec2i>();
						m_grabtimer = GRAB_TRESHOLD;
						m_state = USER_INPUT_SELECTED;
					}
					else
						token->setState(Token::HOVER);
				}
				else
					token->setState(Token::IDLE);
			}

		}
	}
	else
		m_state = END;
}


Prize Level::getPrizeTokensSameTypeFrom(const king::Vec2i& pos)
{

	auto type = m_tokentable[pos.x][pos.y]->getType();


	std::vector<king::Vec2i> verticalconsecutive;
	std::vector<king::Vec2i> horitzontalconsecutive;
	verticalconsecutive.push_back(pos);
	horitzontalconsecutive.push_back(pos);

	//up
	bool consecutive = true;
	for (s32 y = pos.y - 1; (y >= 0) && consecutive; y--)
	{
		if (m_tokentable[pos.x][y]->getType() == type)
			verticalconsecutive.push_back(king::Vec2i(pos.x,y));
		else
			consecutive = false;
	}

	//down
	consecutive = true;
	for (s32 y = pos.y + 1; (y < ROWS) && consecutive; y++)
	{
		if (m_tokentable[pos.x][y]->getType() == type)
			verticalconsecutive.push_back(king::Vec2i(pos.x,y));
		else
			consecutive = false;
	}

	if (verticalconsecutive.size() < 3)
		verticalconsecutive.clear();


	//left
	consecutive = true;
	for (s32 x = pos.x - 1; (x >= 0) && consecutive; x--)
	{
		if (m_tokentable[x][pos.y]->getType() == type)
			horitzontalconsecutive.push_back(king::Vec2i(x,pos.y));
		else
			consecutive = false;
	}

	//right
	consecutive = true;
	for (s32 x = pos.x + 1; (x < COLS) && consecutive; x++)
	{
		if (m_tokentable[x][pos.y]->getType() == type)
			horitzontalconsecutive.push_back(king::Vec2i(x,pos.y));
		else
			consecutive = false;
	}

	if (horitzontalconsecutive.size() < 3)
		horitzontalconsecutive.clear();

	verticalconsecutive.insert(verticalconsecutive.end(),horitzontalconsecutive.begin(), horitzontalconsecutive.end());


	std::vector<std::pair<king::Vec2i, Token * >> prizesvec;
	for (auto &position: verticalconsecutive)
	{
		prizesvec.push_back(std::make_pair(position,m_tokentable[position.x][position.y].get()));
	}

	return Prize(type, prizesvec);


}


std::vector<Prize> Level::getNewPrizes(const std::vector<king::Vec2i>& positions)
{
	std::vector<Prize> prizesfound;
	std::vector<Prize> prizesmerged;

	for (auto &postocheck: positions)
	{

		auto prize = getPrizeTokensSameTypeFrom(postocheck);
		if (!prize.empty())
		{
			//prize.debug();
			prizesfound.push_back(prize);
		}
	}

	for (auto &prize : prizesfound)
	{

		bool contained = false;
		for (auto itprize2 = prizesmerged.begin(); (itprize2 != prizesmerged.end()) && !contained; itprize2++ )
		{

			if (((*itprize2).getType() == prize.getType() ) && ((*itprize2).isContained(prize)))
			{
				(*itprize2).addIndexs( prize.getIndexs() );
				contained = true;
			}
		}

		if (!contained)
		{
			prizesmerged.push_back(prize);
		}
	}


	/*for (auto &p: prizesmerged)
	{
		p.debug();
	}*/

	return prizesmerged;

}

bool Level::isEnded() const {
	return m_ended;
}

std::string Level::getTimeRemainingText()
{
	auto str = std::to_string(m_timeremaining);
	str.erase(str.begin() + 5,str.end());
	return str;
}

::Token::TokenType Level::generateTokenType() {

	auto typecount = Token::MAX_NUM - 1;
	std::uniform_int_distribution<u32> dist(0, typecount);
	Token::TokenType t;
	switch(dist(m_mt))
	{
	case 0:
		t = Token::RED;
		break;

	case 1:
		t = Token::BLUE;
		break;

	case 2:
		t = Token::GREEN;
		break;

	case 3:
		t = Token::YELLOW;
		break;

	case 4:
		t = Token::PURPLE;
		break;

	default:
		KING_merror(std::string("BAD RESULT RANDOM"));
		break;
	}
	return t;
}
