#include <game/Token.h>
#include <core/Exceptions.h>
#include <core/Core.h>
#include <input/InputManager.h>
#include <sound/SoundManager.h>


const std::string RED_FILEPATH("media/Red.png");
const std::string BLUE_FILEPATH("media/Blue.png");
const std::string GREEN_FILEPATH("media/Green.png");
const std::string PURPLE_FILEPATH("media/Purple.png");
const std::string YELLOW_FILEPATH("media/Yellow.png");

const std::string SOUND_ANIMFIN("media/fall.ogg");

const f64 ROT_DEGREESPERSEC_HOVER(360);


const f32 MOVE_ACCELERATIONPERSEC(1400.0);

const f64 ROT_DEGREESPERSEC_PRIZE(0);
const f32 SCALEUP_PERSEC_PRIZE(4.00);
const f32 SCALEUP_MAXSIZE(1.5);
const f32 SCALEDOWN_PERSEC_PRIZE(3.00);

Token::Token(TokenType tokentype,const king::Vec2f& pos, const u32 depthZ):
	m_type(tokentype),
	m_status(IDLE),
	m_endposanim(0,0),
	m_direction(0,0),
	m_distance(0),
	m_currspeed(0),
	m_delayanim(0),
	m_depth(depthZ),
	m_endsound(false)
{

	std::string file;
	switch(m_type)
	{
	case RED:
		file = RED_FILEPATH;
		break;
	case BLUE:
		file = BLUE_FILEPATH;
		break;
	case PURPLE:
		file = PURPLE_FILEPATH;
		break;
	case YELLOW:
		file = YELLOW_FILEPATH;
		break;
	case GREEN:
		file = GREEN_FILEPATH;
		break;
	default:
		KING_merror("BAD TOKENTYPE");
		break;
	}

	m_sprite = king::Sprite::create(file);
	m_sprite->setPos(pos);
	m_sprite->setDepthZ(depthZ);
	m_sprite->setAnchor(king::Vec2f(0.5));

}

Token::~Token() {
}

void Token::render() {

}

void Token::update(f64 dt)
{
	switch(m_status)
	{
	case IDLE:

		break;
	case HOVER:
		m_sprite->setRotation(m_sprite->getRotation() + ( ROT_DEGREESPERSEC_HOVER * dt) );
		break;
	case SELECTED:
		m_sprite->setRotation(m_sprite->getRotation() + ( ROT_DEGREESPERSEC_HOVER * dt) );
		break;
	case MOVING:
	{

		m_delayanim -= dt;
		if (m_delayanim < 0.0)
		{

			auto dist = static_cast<f32>( (m_currspeed * dt) + (0.5* MOVE_ACCELERATIONPERSEC * dt *dt));
			king::Vec2f newpos(m_sprite->getPos().x + dist * m_direction.x,m_sprite->getPos().y + dist * m_direction.y);
			m_sprite->setPos(newpos);
			m_currspeed += MOVE_ACCELERATIONPERSEC * dt;
			m_distance -= dist;
			m_sprite->setVisible(true);
			if (m_distance < 0.0)
			{
				if (m_endsound)
					king::CoreSingleton::get().getSoundMgr().playSound(SOUND_ANIMFIN);
				m_endsound = false;
				setState(Token::IDLE);
				m_sprite->setPos(m_endposanim);
				m_delayanim = 0;


			}
		}
	}
		break;

	case PRIZEANIM_UP:
	{
		auto scale = m_sprite->getScaleX() + (SCALEUP_PERSEC_PRIZE * dt);
		if (scale < SCALEUP_MAXSIZE)
		{
			m_sprite->setScale( scale, scale);
			m_sprite->setRotation(m_sprite->getRotation() + ( ROT_DEGREESPERSEC_PRIZE * dt) );
		}
		else
		{
			m_sprite->setScale(SCALEUP_MAXSIZE, SCALEUP_MAXSIZE);
			m_sprite->setRotation(0);
			setState(Token::PRIZEANIM_DOWN);
		}
	}
		break;
	case GRAB:
		{
			auto &core = king::CoreSingleton::get();
			auto &mouse = core.getInputMgr().getMouse();
			auto mousepos = mouse.getPos<king::Vec2f>();

			m_sprite->setPos(mousepos);
		}
		break;

	case PRIZEANIM_DOWN:
	{
		auto scale = m_sprite->getScaleX() - (SCALEDOWN_PERSEC_PRIZE * dt);
		if (scale > 0.0)
		{
			m_sprite->setScale( scale, scale);
			m_sprite->setRotation(m_sprite->getRotation() + ( ROT_DEGREESPERSEC_PRIZE * dt) );
		}
		else
		{
			m_sprite->setScale( 0, 0);
			m_sprite->setRotation(0);
			setState(Token::END);
		}
	}
		break;
	case END:
		{


		}
			break;
	default:
		break;

	}
}

king::Rectf Token::getCollisionArea() const
{
	return m_sprite->getBoundingBox();
}

void Token::setState(TokenState state) {
	switch(state)
	{
	case IDLE:
		m_sprite->setRotation(0);
		m_sprite->setScale(1,1);
		m_sprite->setDepthZ(m_depth);
		break;
	case HOVER:
		m_sprite->setDepthZ(m_depth);
		break;
	case GRAB:
		m_sprite->setRotation(0);
		m_sprite->setScale(1.2,1.2);
		m_sprite->setDepthZ(m_depth-1);
		break;
	case SELECTED:
		m_sprite->setScale(1.2,1.2);
		m_sprite->setDepthZ(m_depth);
		break;
	case MOVING:
		m_sprite->setRotation(0);
		m_sprite->setScale(1,1);
		if (m_delayanim)
			m_sprite->setVisible(false);
		m_currspeed = 0;
		break;
	case PRIZEANIM_UP:
		m_sprite->setDepthZ(m_depth);
		break;
	case PRIZEANIM_DOWN:
		m_sprite->setDepthZ(m_depth);
		break;
	case END:
		m_sprite->setDepthZ(m_depth);
		break;

	default:
		break;

	}

	m_status = state;
}

Token::TokenType Token::getType()
{
	return m_type;
}

void Token::setPos(const king::Vec2f& pos) {
	m_sprite->setPos(pos);
}

king::Vec2f Token::getPos() const {
	return m_sprite->getPos();
}
void Token::setPosEnd(const king::Vec2f& pos) {

	m_endposanim = pos;
	m_direction = m_endposanim - m_sprite->getPos();
	m_distance = m_direction.length();
	m_direction.normalise();


}

void Token::setVisible(const bool visible)
{
	m_sprite->setVisible(visible);
}

Token::TokenState Token::getState() const {

	return m_status;
}


void Token::setDelayAnimation(const f32 delayseconds)
{
	m_delayanim = delayseconds;
}

void Token::setEndSound(const bool enable) {
	m_endsound = enable;
}
