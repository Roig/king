#ifndef GAME_SCORE_H_
#define GAME_SCORE_H_
#include <render/Text.h>

class Score {
public:
	void reset();
	void udpate(f64 dt);
	Score();
	~Score();
	void addPartialToTotal();
	void addPartialScore(const u32 points);
	void resetPartial();
	void setVisible(const bool v);
	u32  getTotalScore() const;
private:

	u32 m_score;
	u32 m_multiplier;
	u32 m_partialscore;
	std::shared_ptr<king::Text> m_partialtext;
	std::shared_ptr<king::Text> m_multipliertext;
	std::shared_ptr<king::Text> m_scoretext;
	std::shared_ptr<king::Text> m_scoreumbertext;

};

#endif /* GAME_SCORE_H_ */
