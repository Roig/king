#include <game/Prize.h>
#include <iostream>
#include <algorithm>

Prize::Prize(Token::TokenType t, const std::vector<std::pair<king::Vec2i, Token * >>& indexs):m_type(t), m_indexs(indexs), m_animated(false)
{

}

bool Prize::empty()
{
	return m_indexs.empty();
}

bool Prize::isContained(const Prize& p)
{
	for (auto &element: p.m_indexs)
	{
		if (isElementContained(element.first))
			return true;
	}

	return false;

}

bool Prize::isElementContained(const king::Vec2i& element)
{

	for(auto &pos: m_indexs)
	{
		if ( element == pos.first)
			return true;
	}
	return false;
}

void Prize::addIndexs(const std::vector<std::pair<king::Vec2i, Token * >>& indexs)
{
	for(auto &pairindex: indexs)
	{
		if (!isElementContained(pairindex.first))
			m_indexs.push_back(pairindex);
	}
}

void Prize::debug()
{
	std::cout<<"type: "<< static_cast<u32>(m_type)<< " : ";
	for(auto &p: m_indexs)
		std::cout<<"("<<p.first.x<<","<<p.first.y<<")"<<" ";
	std::cout<<std::endl;
}

Token::TokenType Prize::getType()
{
	return m_type;
}

std::vector<std::pair<king::Vec2i, Token * >> Prize::getIndexs() const
{
	return m_indexs;
}

void  Prize::animate()
{
	for (auto &index: m_indexs)
	{
		index.second->setState(Token::PRIZEANIM_UP);
	}

	m_animated=true;
}

bool Prize::isAnimated() {

	/*if (m_animated)
	{
		for (auto &index: m_indexs)
		{
			if (index.second->getState() == Token::END)
				return false;
		}

	}*/
	return m_animated;

}

bool Prize::isFinished()
{

	for (auto &index: m_indexs)
	{
		if (index.second->getState() == Token::END)
			return true;
	}

	return false;
}


size_t Prize::getIndexCount() const
{
	return m_indexs.size();
}

u32 Prize::getScore() {
	switch (m_indexs.size() )
	{

	case 3:
		return 50;
		break;
	case 4:
		return 150;
		break;
	case 5:
		return 350;
		break;
	case 6:
		return 1000;
		break;
	case 7:
		return 2500;
		break;
	default:
		return 5000;
		break;
	}
}
