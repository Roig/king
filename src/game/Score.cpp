#include <game/Score.h>

const std::string FONT_DEFAULT("media/OrbitronBlack.ttf");
const king::Vec2f PARTIALSCORE_TEXT_POS(335,55);
const king::Vec2f MULTI_TEXT_POS(590,55);
const king::Vec2f SCORE_TEXT_POS(70,90);
const king::Vec2f SCORE_TEXTNUM_POS(85,125);
const u32 SCORE_Z(30);
const king::color4b::color4b SCORE_COLOR(251,234,0,255);
const king::color4b::color4b SCORE_PARTIAL_COLOR(251,75,0,255);

Score::Score() : m_score(0), m_multiplier(0), m_partialscore(0)
{

	m_scoretext = king::Text::create(FONT_DEFAULT, 32);
	m_scoretext->setDepthZ(SCORE_Z);
	m_scoretext->setColor(SCORE_COLOR);
	m_scoretext->setPos(SCORE_TEXT_POS);
	m_scoretext->setText("SCORE");
	m_scoretext->setVisible(false);

	m_scoreumbertext = king::Text::create(FONT_DEFAULT, 24);
	m_scoreumbertext->setDepthZ(SCORE_Z);
	m_scoreumbertext->setColor(SCORE_COLOR);
	m_scoreumbertext->setPos(SCORE_TEXTNUM_POS);
	m_scoreumbertext->setText(std::to_string(m_score));
	m_scoreumbertext->setVisible(false);

	m_multipliertext = king::Text::create(FONT_DEFAULT, 32);
	m_multipliertext->setDepthZ(SCORE_Z);
	m_multipliertext->setColor(SCORE_PARTIAL_COLOR);
	m_multipliertext->setPos(MULTI_TEXT_POS);
	m_multipliertext->setText("0");
	m_multipliertext->setVisible(false);


	m_partialtext = king::Text::create(FONT_DEFAULT, 32);
	m_partialtext->setDepthZ(SCORE_Z);
	m_partialtext->setColor(SCORE_PARTIAL_COLOR);
	m_partialtext->setPos(PARTIALSCORE_TEXT_POS);
	m_partialtext->setText("0");
	m_partialtext->setVisible(false);



}

void Score::reset()
{
	m_score = 0;
	resetPartial();
	m_scoreumbertext->setText(std::to_string(m_score));

}

void Score::udpate(f64 dt)
{
	if (m_multiplier)
	{
		m_multipliertext->setVisible(true);
		m_multipliertext->setText("X "+std::to_string(m_multiplier) );
	}
	else
		m_multipliertext->setVisible(false);

	if (m_partialscore)
	{
		m_partialtext->setVisible(true);
		m_partialtext->setText(std::to_string(m_partialscore));
	}
	else
		m_partialtext->setVisible(false);
}

Score::~Score() {

}

void Score::addPartialScore(const u32 points) {

	m_multiplier++;
	if (m_multiplier > 5)
		m_multiplier = 5;

	m_partialscore += points;

}

void Score::resetPartial() {
	m_multiplier = 0;
	m_partialscore = 0;
}

void Score::addPartialToTotal()
{
	m_score += (m_partialscore * m_multiplier);

	m_scoreumbertext->setText(std::to_string(m_score));
	resetPartial();

}

void Score::setVisible(const bool v) {
	m_scoretext->setVisible(v);
	m_scoreumbertext->setVisible(v);
	m_multipliertext->setVisible(v);
}

u32 Score::getTotalScore() const {
	return m_score;
}
