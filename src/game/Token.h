#ifndef GAME_TOKEN_H_
#define GAME_TOKEN_H_

#include "render/Sprite.h"
#include "render/Rectangle.h"
#include <core/Vec.h>
#include <core/Rect.h>

class Token {

public:
	typedef enum
	{
		RED,
		BLUE,
		GREEN,
		YELLOW,
		PURPLE,
		MAX_NUM

	}TokenType;

	typedef enum
	{
		IDLE =0,
		HOVER,
		SELECTED,
		GRAB,
		MOVING,
		PRIZEANIM_UP,
		PRIZEANIM_DOWN,
		END
	}TokenState;

	Token(TokenType tokentype, const king::Vec2f& pos, const u32 depthZ);
	~Token();
	void render();
	void update(f64 dt);

	king::Rectf getCollisionArea() const;
	TokenType getType();

	void setPos(const king::Vec2f& pos);
	king::Vec2f getPos() const;

	void setPosEnd(const king::Vec2f& pos);
	void setDelayAnimation(const f32 delayseconds);

	void setState(TokenState state);
	TokenState getState() const;

	void setVisible(const bool visible);
	void setEndSound(const bool enable);

private:


	TokenType m_type;

	std::shared_ptr<king::Sprite> m_sprite;

	TokenState m_status;
	king::Vec2f m_endposanim;
	king::Vec2f m_direction;
	f32	m_distance;
	f32 m_currspeed;
	f32 m_delayanim;
	u32 m_depth;
	bool m_endsound;

};



#endif /* GAME_TOKEN_H_ */
