#ifndef GAME_PRIZE_H_
#define GAME_PRIZE_H_
#include <game/Token.h>
#include <Core/Vec.h>
#include <vector>

struct Prize
{

	Prize(Token::TokenType t, const std::vector<std::pair<king::Vec2i, Token * >>& indexs);
	bool empty();

	bool isContained(const Prize& p);
	bool isElementContained(const king::Vec2i& element);

	void animate();
	bool isAnimated();
	bool isFinished();
	u32	 getScore();
	void addIndexs(const std::vector<std::pair<king::Vec2i, Token * >>& indexs);
	void debug();
	size_t getIndexCount() const;
	Token::TokenType getType();



	std::vector<std::pair<king::Vec2i, Token * >> getIndexs() const;
	Token::TokenType m_type;
	std::vector<std::pair<king::Vec2i, Token * >> m_indexs;

	bool m_animated;
};


#endif /* GAME_PRIZE_H_ */
