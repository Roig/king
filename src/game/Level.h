#ifndef GAME_LEVEL_H_
#define GAME_LEVEL_H_
#include "core/Types.h"
#include "core/Memory.h"
#include <algorithm>
#include <iostream>
#include <array>
#include <vector>
#include <random>
#include <set>
#include <game/Token.h>
#include <render/Text.h>
#include <game/Prize.h>
#include <game/Score.h>

class Level {
public:
	Level();
	void start();
	~Level();
	void update(f64 dt);
	bool isEnded() const;
	void setVisible(const bool visible);

private:

	std::mt19937 m_mt;
	::Token::TokenType generateTokenType();


	static const s32 ROWS = 8;
	static const s32 COLS = 8;
	std::array<std::array<std::unique_ptr<Token>, ROWS >, COLS > m_tokentable;
	std::array<std::array<king::Vec2f, ROWS >, COLS > m_positiontable;
	std::array<king::Vec2f, COLS > m_starttokenpos;

	std::shared_ptr<king::Sprite>	m_background;
	std::shared_ptr<king::Sprite>	m_tapa;
	std::shared_ptr<king::Sprite>	m_backscore;


	typedef enum {
		INIT = 0,
		USER_INPUT_SELECTION,
		USER_INPUT_GRAB,
		USER_INPUT_SELECTED,
		ANIMATE_SELECTION_FORWARD,
		ANIMATE_SELECTION_BACK,
		CHECK_PRIZES_SELECTION,
		ANIMATE_PRIZES,
		ANIMATE_NEWTOKENS,
		END
	}LevelState;

	LevelState m_state;
	::Token::TokenType getTokenTypeNoPrize(const king::Vec2i& position);
	bool isTokenAdjacent(const king::Vec2i& pos1, const king::Vec2i& pos2);

	void generateTokenTableWithoutPrizes();
	void updateUserInput(f64 dt);
	void updateAnimationSelectedForward(f64 dt);
	void updateAnimationSelectedBack(f64 dt);
	void updateAnimatePrizes(f64 dt);
	void updateAnimateNewTokens(f64 dt);
	void swapTokens(const king::Vec2i& pos1, const king::Vec2i& pos2, const bool endsound= false);
	void swapColumn(s32 col);
	void refillTokenTable();
	void updateUserInputSelected(f64 dt);
	void updateEnd(f64 dt);
	void updateUserGrab(f64 dt);
	bool checkGrab(f64 dt);
	bool isAPrizeAnimating();
	void playNextPrizeAnimation();
	bool allPrizeAnimationsFinished();
	void playNextPrizeSound(const u32 prizecount);

	king::Vec2i getTokenIndexByPositionExclude(const king::Vec2f& pos, bool &found,  const king::Vec2i& excludeindex);
	std::vector<Prize> getNewPrizes(const std::vector<king::Vec2i>& positions);
	Prize getPrizeTokensSameTypeFrom(const king::Vec2i& pos1);
	std::string getTimeRemainingText();

	king::Vec2i m_selected[2];

	std::vector<Prize> m_currentprizes;

	f64 m_timeremaining;
	std::shared_ptr<king::Text> m_timetext;
	std::shared_ptr<king::Text> m_timenumbertext;

	std::shared_ptr<king::Text> 	m_finalscorenum;
	std::shared_ptr<king::Text> 	m_finalscoretext;
	std::shared_ptr<king::Sprite>	m_endbackground;

	bool m_ended;
	f64 m_grabtimer;
	king::Vec2i m_grabstartpos;

	u32 m_prizesshowed;
	Score m_score;

};

#endif /* GAME_LEVEL_H_ */
