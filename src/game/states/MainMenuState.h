#ifndef GAME_STATES_MAINMENUSTATE_H_
#define GAME_STATES_MAINMENUSTATE_H_

#include "core/State.h"
#include "core/Singleton.h"
#include <render/Text.h>
#include <render/Sprite.h>
#include <vector>

class MainMenuState: public king::State
{
public:
	void start() 	override;
	void finish()	override;
	void pause() 	override;
	void resume()	override;
	void update(f64 dt) override;
	virtual ~MainMenuState(){};
private:
	friend king::SingletonHolder<MainMenuState>;
	MainMenuState();
	std::shared_ptr<king::Sprite> m_background;
	std::shared_ptr<king::Text> textstart;
	std::shared_ptr<king::Text> textquit;
	std::shared_ptr<king::Text> textname;

	bool m_insidefirststart;
	bool m_insidefirstquit;
};

typedef king::SingletonHolder<MainMenuState> MainMenuStateSingleton;


#endif /* GAME_STATES_MAINMENUSTATE_H_ */
