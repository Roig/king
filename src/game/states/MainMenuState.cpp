#include <game/states/MainMenuState.h>
#include <iostream>
#include "MainGameState.h"
#include <core/Core.h>
#include <input/InputManager.h>
#include <sound/SoundManager.h>

const king::Vec2f START_BUTTON(400,150);
const king::Vec2f EXIT_BUTTON(430,250);
const king::Vec2f NAME_BUTTON(2,590);

const king::color4b::color4b BUTTON_COLOR_IDLE(king::color4b::WHITE);
const king::color4b::color4b BUTTON_COLOR_HOVER(king::color4b::RED);
const king::color4b::color4b BUTTON_COLOR_NAME(king::color4b::BROWN);

const std::string HOVER_SOUND("media/hoversound.ogg");
const std::string SELECT_SOUND("media/selectbeep.ogg");

MainMenuState::MainMenuState():
	State("MainMenuState"),
	m_insidefirststart(true),
	m_insidefirstquit(true)
{
	m_background = king::Sprite::create("media/BackGround.jpg");
	m_background->setDepthZ(100);

	textstart = king::Text::create("media/OrbitronBlack.ttf", 48);
	textstart->setColor(BUTTON_COLOR_IDLE);
	textstart->setPos(START_BUTTON);
	textstart->setText("START");

	textquit = king::Text::create("media/OrbitronBlack.ttf", 48);
	textquit->setColor(BUTTON_COLOR_IDLE);
	textquit->setPos(EXIT_BUTTON);
	textquit->setText("QUIT");

	textname = king::Text::create("media/OrbitronBlack.ttf", 12);
	textname->setColor(BUTTON_COLOR_NAME);
	textname->setPos(NAME_BUTTON);
	textname->setText("Daniel Guzman");

	textname->setVisible(false);
	textstart->setVisible(false);
	textquit->setVisible(false);
	m_background->setVisible(false);

}

void MainMenuState::start() {
	m_insidefirststart = true;
	m_insidefirstquit = true;
	textname->setVisible(true);
	textstart->setVisible(true);
	textquit->setVisible(true);
	m_background->setVisible(true);

}

void MainMenuState::finish() {
	textname->setVisible(false);
	textstart->setVisible(false);
	textquit->setVisible(false);
	m_background->setVisible(false);
}

void MainMenuState::pause() {
}

void MainMenuState::resume() {
}

void MainMenuState::update(f64 dt)
{

	auto &core = king::CoreSingleton::get();
	auto &mouse = core.getInputMgr().getMouse();
	auto mousepos = mouse.getPos<king::Vec2f>();


	if ( textstart->getBoundingBox().contains( mousepos) )
	{
		if (m_insidefirststart)
		{
			king::CoreSingleton::get().getSoundMgr().playSound(HOVER_SOUND);
			m_insidefirststart = false;
		}

		textstart->setColor(BUTTON_COLOR_HOVER);
		if(mouse.isButtonKeyDown(king::Mouse::LEFT) )
		{
			king::CoreSingleton::get().getSoundMgr().playSound(SELECT_SOUND);
			auto& statemgr = king::CoreSingleton::get().getStateMgr();
			statemgr.changeState(MainGameStateSingleton::get_ptr());
		}

	}
	else
	{
		m_insidefirststart = true;
		textstart->setColor(BUTTON_COLOR_IDLE);
	}

	if ( textquit->getBoundingBox().contains( mousepos) )
	{
		if (m_insidefirstquit)
		{
			king::CoreSingleton::get().getSoundMgr().playSound(HOVER_SOUND);
			m_insidefirstquit = false;
		}
		textquit->setColor(BUTTON_COLOR_HOVER);
		if(mouse.isButtonKeyDown(king::Mouse::LEFT) )
		{
			king::CoreSingleton::get().stop();
		}
	}
	else
	{
		m_insidefirstquit = true;
		textquit->setColor(BUTTON_COLOR_IDLE);
	}



}
