
#ifndef GAME_STATES_MAINGAMESTATE_H_
#define GAME_STATES_MAINGAMESTATE_H_
#include "core/State.h"
#include "core/Singleton.h"
#include "render/Sprite.h"
#include "game/Level.h"

class MainGameState: public king::State {
public:
	~MainGameState();

	void start() 	override;
	void finish()	override;
	void pause() 	override;
	void resume()	override;
	void update(f64 dt) override;

private:
	friend king::SingletonHolder<MainGameState>;
	MainGameState();

	Level m_level;
};

typedef king::SingletonHolder<MainGameState> MainGameStateSingleton;


#endif /* GAME_STATES_MAINGAMESTATE_H_ */
