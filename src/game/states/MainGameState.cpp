#include <game/states/MainGameState.h>
#include <game/states/MainMenuState.h>
#include <core/Core.h>
#include <sound/SoundManager.h>

const std::string MUSIC_BACKGROUND("media/music.ogg");
MainGameState::MainGameState() {
	king::CoreSingleton::get().getSoundMgr().playMusic(MUSIC_BACKGROUND);
}

MainGameState::~MainGameState() {

}

void MainGameState::start() {
	m_level.start();
}

void MainGameState::finish()
{
	m_level.setVisible(false);
}

void MainGameState::pause() {
}

void MainGameState::resume() {
}

void MainGameState::update(f64 dt) {
	m_level.update(dt);
	if (m_level.isEnded())
	{
		auto& statemgr = king::CoreSingleton::get().getStateMgr();
		statemgr.changeState(MainMenuStateSingleton::get_ptr());

	}

}
