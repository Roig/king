#include <sound/SoundManager.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <core/Exceptions.h>

namespace king {

SoundManager::SoundManager() {
}

SoundManager::~SoundManager() {
}

void SoundManager::init() {
	int flags=MIX_INIT_OGG;
	int initted=Mix_Init(flags);
	if((initted&flags) != flags) {
		KING_merror(std::string("Mix_OpenAudio: ") + Mix_GetError() );
	}

	if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
	{
		KING_merror(std::string("Mix_OpenAudio: ") + Mix_GetError() );
	}


}

void SoundManager::playMusic(const std::string& filetoplay)
{
	m_music.reset(Mix_LoadMUS(filetoplay.c_str()));


	if( Mix_PlayMusic(m_music.get(), -1) == -1 )
	{
		KING_merror(std::string("Mix_PlayMusic: ") + Mix_GetError() );
	}
}


void SoundManager::playSound(const std::string& filetoplay) {

	auto soundfound = m_sounds.find(filetoplay);
	if (soundfound == m_sounds.end())
	{
		auto rawptr = Mix_LoadWAV(filetoplay.c_str());

		if(!rawptr) {
			KING_merror(std::string("Mix_LoadWAV: ") + Mix_GetError() );
		}

		m_sounds[filetoplay] = std::unique_ptr<Mix_Chunk, sdl_deleter_functors>(rawptr);
		playChannel(rawptr);
	}
	else
	{
		playChannel(soundfound->second.get());
	}
}

void SoundManager::playChannel(Mix_Chunk* chunk) {
	Mix_PlayChannel(-1,chunk,0);
}

}
