#ifndef SOUND_SOUNDMANAGER_H_
#define SOUND_SOUNDMANAGER_H_
#include <core/Memory.h>
#include <render/SDLUtils.h>
#include <string>
#include <map>

namespace king {

class SoundManager {
public:
	SoundManager();

	void init();
	void playMusic(const std::string& filetoplay);
	void playSound(const std::string& filetoplay);
	~SoundManager();
private:
	void playChannel(Mix_Chunk *);
	std::unique_ptr<Mix_Music,sdl_deleter_functors> m_music;
	std::map<std::string, std::unique_ptr<Mix_Chunk,sdl_deleter_functors>> m_sounds;


};

} /* namespace king */

#endif /* SOUND_SOUNDMANAGER_H_ */
