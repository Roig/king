
#ifndef INPUT_MOUSE_H_
#define INPUT_MOUSE_H_
#include "core/Vec.h"
#include <array>
namespace king {



class Mouse {
public:

	Mouse();
	~Mouse();
	void update();

	template <typename T>
	T getPos() const
	{
		T aux(m_pos.x, m_pos.y);
		return aux;
	}

	Vec2i	getPosRel() const;

	typedef enum
	{
		LEFT= 0,
		RIGHT,
		MAX_BUTTONS
	}Buttons;
	void debug();

	//Returns true if the key for the given button is pressed down / false if the key isn't
	bool	isButtonPushed(Buttons button);
	//Returns true if the key for the given button is Key Up
	bool	isButtonKeyUp(Buttons button);
	//Returns true if the key for the given button is first keydown
	bool	isButtonKeyDown(Buttons button);
private:
	typedef enum
	{
		UP = 0,
		DOWN,
		PUSHREPEAT,
		RELEASED
	}ButtonStatus;


	Vec2i		m_pos;
	Vec2i		m_relativepos;

	ButtonStatus m_left;
	ButtonStatus m_right;

	bool isButtonState(Buttons button, ButtonStatus buttstate);
	void updateButtonStatus(ButtonStatus &button, u32 mask );

};

} /* namespace king */

#endif /* INPUT_MOUSE_H_ */
