
#include <input/Mouse.h>
#include <SDL2/SDL.h>
#include <iostream>

namespace king {

Mouse::Mouse() :m_left(UP),m_right(UP)
{

}

Mouse::~Mouse() {
}

void Mouse::updateButtonStatus(ButtonStatus &button, u32 mask )
{
	if (mask)
	{
		if (button == UP)
			button = DOWN;
		else
			button = PUSHREPEAT;
	}
	else
	{
		if (button == PUSHREPEAT)
			button = RELEASED;
		else
			button = UP;
	}
}

void Mouse::update() {
	//Get current mouse state
	auto currentMouseState = SDL_GetMouseState(&m_pos.x, &m_pos.y);
	SDL_GetRelativeMouseState(&m_relativepos.x, &m_relativepos.y);

	updateButtonStatus(m_left, currentMouseState & SDL_BUTTON_LMASK);
	updateButtonStatus(m_right, currentMouseState & SDL_BUTTON_RMASK);
}



Vec2i Mouse::getPosRel() const {
	return m_relativepos;
}

void Mouse::debug() {
//	std::cout<<"Mouse pos: "<<getPos().x<<","<<getPos().y<<" Left: "<<static_cast<u32>(m_left)<<" Right: "<<static_cast<u32>(m_right)<<std::endl;

}

bool Mouse::isButtonPushed(Buttons button) {
	if ( isButtonState(button, PUSHREPEAT) || isButtonState(button, DOWN) )
		return true;
	return false;
}

bool Mouse::isButtonKeyUp(Buttons button) {
	return isButtonState(button, UP);
}

bool Mouse::isButtonKeyDown(Buttons button) {
	return isButtonState(button, DOWN);
}

bool Mouse::isButtonState(Buttons button, ButtonStatus buttstate)
{
	switch(button)
	{
	case LEFT:
		if ( m_left == buttstate) return true;
		break;

	case RIGHT:
		if ( m_right == buttstate) return true;
		break;
	default:
		break;
	}

	return false;
}


} /* namespace king */
