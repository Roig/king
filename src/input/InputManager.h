#ifndef INPUT_INPUTMANAGER_H_
#define INPUT_INPUTMANAGER_H_
#include "Mouse.h"

namespace king {

class InputManager {
public:
	InputManager();
	~InputManager();
	void update();

	Mouse& getMouse();
private:
	Mouse m_mouse;


};

} /* namespace king */

#endif /* INPUT_INPUTMANAGER_H_ */
