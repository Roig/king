#include <input/InputManager.h>
#include <SDL2/SDL.h>
#include <core/Core.h>
namespace king {

InputManager::InputManager() {
}

InputManager::~InputManager() {
}

void InputManager::update()
{
	SDL_Event event;

	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
		case SDL_QUIT:
			king::CoreSingleton::get().stop();
			return;

		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_ESCAPE)
			{
				king::CoreSingleton::get().stop();
				return;
			}
			break;

		default:
			break;

		}
	}

	m_mouse.update();
	//m_mouse.debug();
}

Mouse& InputManager::getMouse() {
	return m_mouse;
}

} /* namespace king */
