#ifndef RENDER_SDLUTILS_H_
#define RENDER_SDLUTILS_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

namespace king {


	struct sdl_deleter_functors
	{
		void operator()(SDL_Window *p) 		const { SDL_DestroyWindow(p); 	}
		void operator()(SDL_Renderer *p) 	const { SDL_DestroyRenderer(p); }
		void operator()(SDL_Texture *p) 	const { SDL_DestroyTexture(p); 	}
		void operator()(SDL_Surface *p) 	const { SDL_FreeSurface(p); 	}
		void operator()(TTF_Font *p) 		const { TTF_CloseFont(p); 		}
		void operator()(Mix_Music *p) 		const { Mix_FreeMusic(p); 		}
		void operator()(Mix_Chunk *p) 		const { Mix_FreeChunk(p); 		}


	};

}


#endif /* RENDER_SDLUTILS_H_ */
