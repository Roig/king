#ifndef RENDER_FONT_H_
#define RENDER_FONT_H_
#include <core/Memory.h>
#include <string>
#include "SDLUtils.h"
#include "core/Rect.h"
#include <render/Color.h>


namespace king {
class Texture;

class Font {
public:
	Font(SDL_Renderer * sdlrender,const u32 size, const std::string& filename);
	std::shared_ptr<Texture> renderText(const std::string &message, const color4b::color4b color);
	~Font();

private:
	void loadFromFile();
	SDL_Renderer * m_sdlrenderer;
	u32 m_size;
	std::string m_file;
	std::unique_ptr<TTF_Font, sdl_deleter_functors> m_ttffont;
};

} /* namespace king */

#endif /* RENDER_FONT_H_ */
