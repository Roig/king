#ifndef RENDER_RECTANGLE_H_
#define RENDER_RECTANGLE_H_
#include <core/Rect.h>
#include "render/Renderable.h"
#include "core/Memory.h"
#include <render/rendertasks/RectangleTask.h>
#include <render/Color.h>


namespace king {

class Rectangle : public Renderable {
public:
	static std::shared_ptr<Rectangle> create(const Rectf& rect);

	void draw(Renderer *renderer);
	Rectf getRect() const;
	void setRect(Rectf rect);

	virtual ~Rectangle();
private:
	Rectangle(const Rectf& rect);
	Rectf m_rect;
	std::shared_ptr<RectangleTask> m_task;
};

} /* namespace king */

#endif /* RENDER_RECTANGLE_H_ */




