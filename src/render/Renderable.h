
#ifndef RENDER_RENDERABLE_H_
#define RENDER_RENDERABLE_H_

#include "core/Types.h"
#include "core/Vec.h"

namespace king {

class Renderer;

class Renderable {
public:
	Renderable();
	virtual ~Renderable();

	void setPos(const Vec2f& pos);
	Vec2f getPos() const;

	void setDepthZ(const u32 depth);
	u32 getDepthZ() const;

	void setScale(const f32 scaleX, const f32 scaleY);
	void setScaleX(const f32 scaleX);
	void setScaleY(const f32 scaleY);
	f32 getScaleX() const;
	f32 getScaleY() const;

	void setRotation(const f32 rotation);
	f32 getRotation() const;

	void setVisible(const bool isvisible);
	bool isVisible() const;

	void setAnchor(const Vec2f & anchorpoint);
	Vec2f getAnchor() const;

	void setSize(const Vec2f& size);
	Vec2f getSize() const;

	virtual void draw(Renderer *) = 0;

private:

	Vec2f 	m_position;
	u32 	m_depthZ;
	Vec2f 	m_scale;
	f32 	m_rotdegrees;
	bool 	m_isvisible;
	Vec2f	m_anchorpoint;
	Vec2f	m_size;
};

} /* namespace king */

#endif /* RENDER_RENDERABLE_H_ */
