#ifndef COLOR_H_
#define COLOR_H_

#include <core/Vec.h>
namespace king
{
    namespace color4b
    {
    	typedef Vec4b color4b;
		static const color4b WHITE (255, 255, 255, 255);
		static const color4b YELLOW (255, 255, 0, 255);
		static const color4b GREEN ( 0, 255, 0, 255);
		static const color4b BLUE ( 0, 0, 255, 255);
		static const color4b RED (255, 0, 0, 255);
		static const color4b MAGENTA(255, 0, 255, 255);
		static const color4b BLACK ( 0, 0, 0, 255);
		static const color4b ORANGE (255, 127, 0, 255);
		static const color4b GRAY (166, 166, 166, 255);
		static const color4b BROWN (160,82,45, 255);
	}
}    
#endif /* COLOR_H_ */    
