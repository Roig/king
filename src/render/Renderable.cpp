#include <render/Renderable.h>
#include <cmath>

namespace king {

Renderable::Renderable():
		m_position(0.0,0.0),
		m_depthZ(0),
		m_scale(1.0,1.0),
		m_rotdegrees(0.0),
		m_isvisible{true},
		m_anchorpoint(0,0),
		m_size(0,0)
	{
	}

	Renderable::~Renderable()
	{
	}

	void Renderable::setPos(const Vec2f& pos)
	{
		m_position = pos;
	}

	Vec2f Renderable::getPos() const
	{
		return m_position;
	}

	void Renderable::setDepthZ(const u32 depth)
	{
		m_depthZ = depth;
	}

	u32 Renderable::getDepthZ() const
	{
		return m_depthZ;
	}


	void Renderable::setScale(const f32 scaleX, const f32 scaleY)
	{
		m_scale.x = scaleX;
		m_scale.y = scaleY;
	}

	void Renderable::setScaleX(const f32 scaleX)
	{
		m_scale.x = scaleX;
	}

	void Renderable::setScaleY(const f32 scaleY)
	{
		m_scale.y = scaleY;
	}

	f32 Renderable::getScaleX() const
	{
		return m_scale.x;
	}

	f32 Renderable::getScaleY() const
	{
		return m_scale.y;
	}

	void Renderable::setRotation(const f32 rotation)
	{

		m_rotdegrees = fmod(rotation,360.0);
	}

	f32 Renderable::getRotation() const
	{
		return m_rotdegrees;
	}

	void Renderable::setVisible(const bool isvisible)
	{
		m_isvisible = isvisible;
	}

	bool Renderable::isVisible() const
	{
		return m_isvisible;
	}

	void Renderable::setAnchor(const Vec2f& anchorpoint)
	{
		m_anchorpoint = anchorpoint;
	}

	Vec2f Renderable::getAnchor() const {
		return m_anchorpoint;
	}

	void Renderable::setSize(const Vec2f& size)
	{
		m_size = size;
	}

	Vec2f Renderable::getSize() const
	{
		return m_size;
	}

} /* namespace king */
