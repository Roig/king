
#ifndef RENDER_SPRITE_H_
#define RENDER_SPRITE_H_
#include "render/Renderable.h"
#include <string>
#include "core/Memory.h"
#include "Texture.h"


namespace king {

class TextureTask;

class Sprite : public Renderable
{
public:
	static std::shared_ptr<Sprite> create();
	static std::shared_ptr<Sprite> create(const std::string& filename);

	void initFile(const std::string& filename );
	void draw(Renderer *);


	void setFlip( const Texture::Flipping flip );
	Texture::Flipping getFlip() const;

	void setClip( const Rectu& clip );
	Rectu getClip() const;

	Rectf getBoundingBox() const;

	~Sprite();

private:
	Sprite();

	Texture::Flipping 	m_flipping;
	Rectu	m_clip;
	std::shared_ptr<Texture> m_texture;
	std::shared_ptr<TextureTask> m_task;

};

} /* namespace king */

#endif /* RENDER_SPRITE_H_ */
