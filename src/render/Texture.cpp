
#include <core/Exceptions.h>
#include <render/Texture.h>
#include <SDL2/SDL_image.h>

namespace king {

Texture::Texture(SDL_Renderer * sdlrender):
		m_sdlrenderer(sdlrender),m_sizerect(0)
{
}

void Texture::loadFromFile(const std::string& file)
{
	std::unique_ptr<SDL_Surface, sdl_deleter_functors> image_surface(IMG_Load(file.c_str()));
	if(!image_surface) {
		KING_merror(std::string("IMG_Load: ") + IMG_GetError());
	}

	m_sdltexture.reset(SDL_CreateTextureFromSurface(m_sdlrenderer, image_surface.get()));
	if (!m_sdltexture) {
		KING_merror(std::string("CreateTextureFromSurface ") + SDL_GetError() );
	}

	setTextureParams();
}

void Texture::loadFromSDLSurface(std::unique_ptr<SDL_Surface, sdl_deleter_functors> surf)
{
	m_sdltexture.reset(SDL_CreateTextureFromSurface(m_sdlrenderer, surf.get()));
	if (!m_sdltexture){
		KING_merror(std::string("loadFromSDLSurface ") + SDL_GetError() );
	}

	setTextureParams();
}

void Texture::setTextureParams()
{
	int w,h;
	unsigned int intbpp;
	SDL_QueryTexture(m_sdltexture.get(), &intbpp, NULL, &w, &h);

	m_sizerect.w = w;
	m_sizerect.h = h;

}

Texture::~Texture() {

}


SDL_Texture* Texture::getSDLTexture()
{
	return m_sdltexture.get();
}

u32 Texture::getWidth() const {
	return m_sizerect.w;
}

u32 Texture::getHeight() const {
	return m_sizerect.h;
}

Rectu Texture::getRect() const {
	return m_sizerect;
}

}
