
#ifndef RENDER_TEXTURE_H_
#define RENDER_TEXTURE_H_

#include <core/Memory.h>
#include <string>
#include "SDLUtils.h"
#include "core/Rect.h"

namespace king {

class Texture
{
public:

	typedef enum
	{
		FLIP_NONE = 0,
		FLIP_HORIZONTAL,
		FLIP_VERTICAL,
		FLIP_VERTICAL_AND_HORIZONTAL
	}Flipping;

	Texture(SDL_Renderer * sdlrender);
	void loadFromFile(const std::string& file);
	void loadFromSDLSurface(std::unique_ptr<SDL_Surface, sdl_deleter_functors> surf);
	SDL_Texture* getSDLTexture();

	u32 getWidth() const;
	u32 getHeight() const;
	Rectu getRect() const;

	~Texture();
private:
	void setTextureParams();
	std::unique_ptr<SDL_Texture, sdl_deleter_functors> m_sdltexture;
	SDL_Renderer * m_sdlrenderer;
	Rectu m_sizerect;
};

} /* namespace king */

#endif /* RENDER_TEXTURE_H_ */
