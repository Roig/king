
#include <render/Rectangle.h>
#include <core/Core.h>
#include <render/RenderManager.h>
#include <render/Renderer.h>

namespace king {

std::shared_ptr<Rectangle> Rectangle::create(const Rectf& rect)
{
	std::shared_ptr<Rectangle> ptr;
	ptr.reset(new Rectangle(rect));
	CoreSingleton::get().getRenderMgr().registerRenderable(ptr);
	return ptr;
}

void Rectangle::draw(Renderer* renderer)
{
	if (isVisible())
	{
		m_task->init(getDepthZ(),getRect(), getAnchor(),getScaleX(), getScaleY(), color4b::WHITE);
		renderer->addTask(m_task);
	}

}

void Rectangle::setRect(Rectf rect)
{
	m_rect = rect;
}

Rectf Rectangle::getRect() const {
	return m_rect;
}

Rectangle::Rectangle(const Rectf& rect): m_rect(rect)
{
	m_task = std::make_shared<RectangleTask>();
}

Rectangle::~Rectangle()
{

}

} /* namespace king */
