#ifndef RENDER_RENDERER_H_
#define RENDER_RENDERER_H_

#include <core/Memory.h>
#include <core/Types.h>
#include "SDLUtils.h"
#include <vector>
#include <map>

namespace king {
class RenderTask;

class Renderer {
public:
	Renderer(SDL_Window * window);
	~Renderer();

	void render();
	void addTask(std::shared_ptr<RenderTask> task);

	void clear();

	SDL_Renderer* getSDLRenderer();
private:
	std::unique_ptr<SDL_Renderer, sdl_deleter_functors> m_sdlrenderer;


	std::map<u32, std::vector<std::shared_ptr<RenderTask> >, std::greater<u32> > m_queue;

};

} /* namespace king */

#endif /* RENDER_RENDERER_H_ */
