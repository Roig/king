#include <render/Sprite.h>
#include <render/Texture.h>
#include <core/Core.h>
#include <render/RenderManager.h>
#include <render/rendertasks/TextureTask.h>
#include <render/Renderer.h>

namespace king {

Sprite::Sprite():
m_flipping(Texture::FLIP_NONE), m_clip(0)
{

	m_task = std::make_shared<TextureTask>();
}

Sprite::~Sprite() {

}

void Sprite::draw(Renderer * renderer) {


	if (isVisible())
	{
		m_task->init(getDepthZ(),m_texture, getPos(), getAnchor(), getSize(),getClip(), getScaleX(), getScaleY(),getRotation(), getFlip());
		renderer->addTask(m_task);
	}

}


void Sprite::initFile(const std::string& filename)
{
	m_texture = CoreSingleton::get().getRenderMgr().createTexture(filename);
	m_texture->loadFromFile(filename);

	m_clip = m_texture->getRect();
	setSize(Vec2f(m_clip.w,m_clip.h));


}

std::shared_ptr<Sprite> Sprite::create()
{
	std::shared_ptr<Sprite> ptr;
	ptr.reset(new Sprite);

	CoreSingleton::get().getRenderMgr().registerRenderable(ptr);


	return ptr;
}

std::shared_ptr<Sprite> Sprite::create(const std::string& filename) {
	auto ptr = create();
	ptr->initFile(filename);
	return ptr;
}

void Sprite::setFlip(const Texture::Flipping flip) {
	m_flipping=flip;
}

Texture::Flipping Sprite::getFlip() const {
	return m_flipping;
}

void Sprite::setClip(const Rectu& clip) {
	m_clip = clip;
}

Rectu Sprite::getClip() const {
	return m_clip;
}

Rectf Sprite::getBoundingBox() const
{
	Rectf box;
	box.x = getPos().x - (getSize().x * getScaleX() * getAnchor().x);
	box.y = getPos().y - (getSize().y * getScaleY() * getAnchor().y);
	box.w = getSize().x * getScaleX();
	box.h = getSize().y * getScaleY();
	return box;
}


} /* namespace king */

