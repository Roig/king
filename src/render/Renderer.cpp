#include <core/Exceptions.h>
#include <render/Renderer.h>
#include "rendertasks/RenderTask.h"
#include <algorithm>
#include <SDL2/SDL.h>
#include <iostream>

namespace king {


Renderer::Renderer(SDL_Window * window)
{
	m_sdlrenderer.reset(SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED));
	if (!m_sdlrenderer) {
		KING_merror(std::string("SDL: Failed to create a valid SDL renderer. SDLERROR=") + SDL_GetError());
	}

	SDL_SetRenderDrawColor(m_sdlrenderer.get(),0 , 0, 0, 255);
}

Renderer::~Renderer()
{

}



SDL_Renderer* Renderer::getSDLRenderer() {
	return m_sdlrenderer.get();
}


void Renderer::render()
{
	clear();

	//render the tasks
	SDL_SetRenderTarget( m_sdlrenderer.get(), NULL);


	//std::cout<< "-- start rendering -- " << std::endl;
	for( auto &itvec: m_queue)
	{
		//std::cout<< "Z = " << itvec.first<< "qty: " << itvec.second.size()<<std::endl;
		for (auto &ittask: itvec.second)
			ittask->render(this);
	}


	//clear tasks
	m_queue.clear();

	//Swap buffers
	SDL_RenderPresent(m_sdlrenderer.get());

}

void Renderer::addTask(std::shared_ptr<RenderTask> ptask) {
	m_queue[ptask->get_depthZ()].push_back(ptask);
}


void Renderer::clear()
{
	SDL_SetRenderDrawColor(m_sdlrenderer.get(),0, 0, 0, 255);
	SDL_RenderClear(m_sdlrenderer.get());
}

} /* namespace king */
