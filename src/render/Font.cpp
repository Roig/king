
#include <render/Font.h>
#include <SDL2/SDL_ttf.h>
#include <core/Exceptions.h>
#include <render/Texture.h>
#include <core/Core.h>
#include <render/RenderManager.h>
#include <render/rendertasks/TextureTask.h>
#include <render/Renderer.h>

namespace king {


Font::Font(SDL_Renderer* sdlrender, const u32 size, const std::string& f): m_sdlrenderer(sdlrender), m_size(size), m_file(f)
{
	loadFromFile();
}

void Font::loadFromFile()
{
	//Open the font
	m_ttffont.reset(TTF_OpenFont(m_file.c_str(),m_size));
	if (m_ttffont == nullptr){
		KING_merror(std::string("TTF_OpenFont error ") );
	}


}

std::shared_ptr<Texture> Font::renderText(const std::string& message,const color4b::color4b color)
{
	std::unique_ptr<SDL_Surface, sdl_deleter_functors> surf;
	SDL_Color c = {color.r,color.g,color.b,color.a};
	surf.reset(TTF_RenderText_Blended(m_ttffont.get(), message.c_str(), c));

	if (!surf){
		KING_merror(std::string("TTF_RenderText_Blended error ")+ TTF_GetError() );
	}

	auto textureptr = CoreSingleton::get().getRenderMgr().createTexture();
	textureptr->loadFromSDLSurface(std::move(surf));
	return textureptr;

}

Font::~Font() {
	// TODO Auto-generated destructor stub
}

} /* namespace king */
