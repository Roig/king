#ifndef RENDER_TEXT_H_
#define RENDER_TEXT_H_

#include <render/Renderable.h>
#include <string>
#include "core/Memory.h"
#include "Texture.h"
#include <render/Color.h>

namespace king {
class Font;
class TextureTask;
class Text: public Renderable {
public:
	static std::shared_ptr<Text> create(const std::string& fontfilename, const u32 size);

	void draw(Renderer * renderer);
	void setText(const std::string& text);
	Rectf getBoundingBox() const;

	void setColor(const color4b::color4b c);
	color4b::color4b getColor() const;
	~Text();
private:
	Text() = delete;
	Text(const std::string& fontfilename,	const u32 size);
	std::shared_ptr<Font> m_font;
	std::shared_ptr<Texture> m_texture;
	std::shared_ptr<TextureTask> m_task;
	color4b::color4b m_color;
	std::string m_textstr;
};

} /* namespace king */

#endif /* RENDER_TEXT_H_ */
