#include "TextureTask.h"
#include "render/Renderer.h"
#include <SDL2/SDL.h>
#include <iostream>
#include "render/Texture.h"
namespace king {

TextureTask::TextureTask():
RenderTask(0),
m_texture(nullptr),
m_position(0,0),
m_scaleX(0),
m_scaleY(0),
m_rotdegrees(0),
m_clip(0),
m_flip(Texture::FLIP_NONE),
m_anchor(0),
m_contentSize(0)
{
}

TextureTask::~TextureTask()
{
}


void TextureTask::debug() {
	std::cout<< "pos: " <<m_position.x << ", "<< m_position.y<<std::endl;
	std::cout<< "clip: " <<m_clip.x << ", "<< m_clip.y<<", "<< m_clip.w<< ", "<< m_clip.h<<std::endl;
	std::cout<< "scale: " <<m_scaleX << ", "<< m_scaleY<<std::endl;
	std::cout<< "rot: " <<m_rotdegrees<<std::endl;
}

void TextureTask::render(Renderer* renderer) {




	SDL_Rect sdl_src_rect;
	sdl_src_rect.x = m_clip.x;
	sdl_src_rect.y = m_clip.y;
	sdl_src_rect.w = m_clip.w;
	sdl_src_rect.h = m_clip.h;

	SDL_Rect sdl_dest_rect;
	sdl_dest_rect.x = m_position.x - ( m_scaleX * m_contentSize.w * m_anchor.x);
	sdl_dest_rect.y = m_position.y - ( m_scaleY * m_contentSize.h * m_anchor.y);
	sdl_dest_rect.w = m_contentSize.w * m_scaleX;
	sdl_dest_rect.h = m_contentSize.h * m_scaleY;


	int result;

	if ((m_flip != Texture::FLIP_NONE) || (m_rotdegrees))
	{

		SDL_Point rot_center;
		rot_center.x = m_contentSize.w * m_anchor.x * m_scaleX;
		rot_center.y = m_contentSize.h * m_anchor.y * m_scaleY;

		SDL_RendererFlip sdl_flip = SDL_FLIP_NONE;

		switch(m_flip)
		{
			case Texture::FLIP_HORIZONTAL:
				sdl_flip = SDL_FLIP_HORIZONTAL;
				break;
			case Texture::FLIP_VERTICAL:
				sdl_flip = SDL_FLIP_VERTICAL;
				break;
			case Texture::FLIP_VERTICAL_AND_HORIZONTAL:
				sdl_flip = (SDL_RendererFlip) (SDL_FLIP_HORIZONTAL | SDL_FLIP_HORIZONTAL) ;
				break;
			case Texture::FLIP_NONE:
			default:
				break;
		}

		result = SDL_RenderCopyEx(renderer->getSDLRenderer(), m_texture->getSDLTexture(), &sdl_src_rect, &sdl_dest_rect, m_rotdegrees, &rot_center, sdl_flip );
	}
	else
	{
		result = SDL_RenderCopy(renderer->getSDLRenderer(), m_texture->getSDLTexture(), &sdl_src_rect, &sdl_dest_rect);
	}

	if (result != 0)
		std::cout<<"texture::render SDL_RenderCopy failed: "<< std::string(SDL_GetError()) << std::endl;
}

void TextureTask::init(u32 depthZ,const std::shared_ptr<Texture>& texture,
		const Vec2f& pos,const Vec2f& anchor, const Vec2f& contentSize, const Rectu& clip, const f32 scaleX, const f32 scaleY,
		const f32 rotdegrees, const Texture::Flipping flip) {
	m_depthZ = depthZ;
	m_texture=texture;
	m_position=pos;
	m_scaleX=scaleX;
	m_scaleY=scaleY;
	m_rotdegrees=rotdegrees;
	m_flip=flip;
	m_clip=clip;
	m_anchor = anchor;
	m_contentSize = contentSize;
}

} /* namespace king */
