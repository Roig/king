
#ifndef RENDER_RENDERTASKS_RENDERTASK_H_
#define RENDER_RENDERTASKS_RENDERTASK_H_

#include <core/Memory.h>
#include <core/Types.h>


namespace king {

class Renderer;
class RenderTask
{
public:
	virtual ~RenderTask(){};
	inline u32 get_depthZ() const { return m_depthZ; }
	virtual void debug() {} ;
	virtual void render(Renderer *) = 0;

protected:
	RenderTask(const u32 depth):m_depthZ(depth){};
	u32 m_depthZ;


};




} /* namespace king */

#endif /* RENDER_RENDERTASKS_RENDERTASK_H_ */
