#include "RectangleTask.h"
#include "render/Renderer.h"
#include <SDL2/SDL.h>
#include <iostream>

namespace king {
	RectangleTask::RectangleTask():
				RenderTask(0),
				m_rect(0),
				m_scaleX(0),
				m_scaleY(0),
				m_color(color4b::BLACK),
				m_fill(false),
				m_anchor(0)

	{
	}


RectangleTask::~RectangleTask()
{
}


void RectangleTask::debug() {
std::cout<< "rect: " <<m_rect.x << ", "<< m_rect.y<<", "<<m_rect.w << ", "<< m_rect.h<<std::endl;
		std::cout<< "m_color: " <<(int)m_color.r << ", "<<(int) m_color.g<< ", "<<(int) m_color.b<< ", "<< (int)m_color.a<<std::endl;
}

void RectangleTask::render(Renderer* renderer) {
	SDL_Rect sdlrect;
	sdlrect.x = m_rect.x - (m_scaleX * m_rect.w * m_anchor.x);
	sdlrect.y = m_rect.y - (m_scaleY * m_rect.h * m_anchor.y);
	sdlrect.w = m_rect.w * m_scaleX;
	sdlrect.h = m_rect.h * m_scaleY;

	SDL_SetRenderDrawColor(renderer->getSDLRenderer(),m_color.r,m_color.g,m_color.b,m_color.a);

	int result = 1;
	if (m_fill)
		result = SDL_RenderFillRect(renderer->getSDLRenderer(), &sdlrect );
	else
		result = SDL_RenderDrawRect(renderer->getSDLRenderer(), &sdlrect );

	if (result != 0)
		std::cout<<"SDL_RenderDrawRect failed: "<< std::string(SDL_GetError()) << std::endl;
}

void RectangleTask::init(u32 depthZ,const Rectf& rectangle,const Vec2f& anchor,const f32 scaleX, const f32 scaleY, const color4b::color4b &color, bool fill )
{
	m_color = color;
	m_depthZ = depthZ;
	m_rect	= rectangle;
	m_scaleX = scaleX;
	m_scaleY = scaleY;
	m_fill = fill;
	m_anchor = anchor;
}

} /* namespace king */

