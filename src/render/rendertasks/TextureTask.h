#ifndef RENDER_RENDERTASKS_TEXTURETASK_H_
#define RENDER_RENDERTASKS_TEXTURETASK_H_

#include <core/Rect.h>
#include "RenderTask.h"
#include "render/Texture.h"


namespace king {

class TextureTask: public RenderTask {
public:
	TextureTask();
	virtual ~TextureTask();
	virtual void debug();
	virtual void render(Renderer * renderer);

	void init(u32 depthZ,const std::shared_ptr<Texture>& texture,const Vec2f& pos, const Vec2f& anchor, const Vec2f& contentSize, const Rectu& clip, const f32 scaleX, const f32 scaleY, const f32 rotdegrees, const Texture::Flipping flip);

private:
	std::shared_ptr<Texture> m_texture;
	Vec2f 	m_position;
	f32		m_scaleX;
	f32		m_scaleY;
	f32		m_rotdegrees;
	Rectu	m_clip;
	Texture::Flipping m_flip;
	Vec2f	m_anchor;
	Vec2f	m_contentSize;

};

} /* namespace king */

#endif /* RENDER_RENDERTASKS_TEXTURETASK_H_ */
