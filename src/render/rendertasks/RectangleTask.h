#ifndef RECTANGLE_TASK_H_
#define RECTANGLE_TASK_H_

#include <render/rendertasks/RenderTask.h>
#include <core/Rect.h>
#include <render/Color.h>

namespace king
{

	class RectangleTask : public RenderTask
	{
	public:
		RectangleTask();
		virtual ~RectangleTask();
		virtual void debug();
		virtual void render(Renderer * renderer);
		void init(u32 depthZ,const Rectf& rectangle,const Vec2f& anchor, const f32 scaleX, const f32 scaleY, const color4b::color4b &color, bool fill = false);

    private:
		Rectf m_rect;
		f32	m_scaleX;
		f32	m_scaleY;
		color4b::color4b m_color;
		bool m_fill;
		Vec2f m_anchor;
	};
}
#endif /* RECTANGLE_TASK_H_ */
