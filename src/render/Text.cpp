#include <render/Text.h>
#include <render/Texture.h>
#include <core/Core.h>
#include <render/RenderManager.h>
#include <render/rendertasks/TextureTask.h>
#include <render/Renderer.h>
#include <render/Font.h>

namespace king {

std::shared_ptr<Text> Text::create(const std::string& fontfilename,	const u32 size)
{
	std::shared_ptr<Text> ptr;
	ptr.reset(new Text(fontfilename, size));
	CoreSingleton::get().getRenderMgr().registerRenderable(ptr);
	return ptr;

}

void Text::draw(Renderer* renderer)
{
	if (isVisible() && m_texture)
	{

		m_task->init(getDepthZ(),m_texture, getPos(), getAnchor(), getSize(),m_texture->getRect(), getScaleX(), getScaleY(),getRotation(), Texture::FLIP_NONE);
		renderer->addTask(m_task);
	}

}

void Text::setText(const std::string& text)
{
	m_textstr = text;
	if (!m_textstr.empty())
	{
		m_texture = m_font->renderText(m_textstr,m_color);
		auto rect = m_texture->getRect();
		setSize(Vec2f(rect.w,rect.h));
	}

}

Rectf Text::getBoundingBox() const {
	Rectf box;
	box.x = getPos().x - (getSize().x * getScaleX() * getAnchor().x);
	box.y = getPos().y - (getSize().y * getScaleY() * getAnchor().y);
	box.w = getSize().x * getScaleX();
	box.h = getSize().y * getScaleY();

	return box;
}


Text::Text(const std::string& fontfilename,	const u32 size) {
	m_color= color4b::WHITE;
	m_font = CoreSingleton::get().getRenderMgr().createFont(fontfilename, size);
	m_task = std::make_shared<TextureTask>();
}

Text::~Text() {
}

void Text::setColor(const color4b::color4b c)
{
	if (m_color != c)
	{
		m_color=c;
		setText(m_textstr);
	}
}

color4b::color4b Text::getColor() const {
	return m_color;
}
} /* namespace king */


