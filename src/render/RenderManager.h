#ifndef RENDERMANAGER_H_
#define RENDERMANAGER_H_

#include <core/Memory.h>
#include <core/Types.h>
#include <string>
#include <list>
#include <map>

#include "SDLUtils.h"

namespace king {

class Texture;
class Renderer;
class Renderable;
class Font;
class RenderManager
{

public:


	void init(const std::string &windowTitle, const u32 w, const u32 h, const bool fullscreen);

	void render();

	std::shared_ptr<Texture> createTexture(const std::string& filename);
	std::shared_ptr<Texture> createTexture();

	std::shared_ptr<Font> createFont(const std::string& filename, const u32 size);

	void registerRenderable(const std::weak_ptr<Renderable>& renderable);

	RenderManager();
	~RenderManager();

private:
	std::unique_ptr<SDL_Window, sdl_deleter_functors> m_sdlwindow;
	std::unique_ptr<Renderer> m_renderer;

	std::list<std::weak_ptr<Renderable>> m_renderables;
	std::map<std::string, std::shared_ptr<Texture>>	m_textures;
	std::map<std::string, std::map<u32,std::shared_ptr<Font>>>	m_fontsloaded;

};

}
#endif /* RENDERMANAGER_H_ */
