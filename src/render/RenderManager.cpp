#include <core/Exceptions.h>
#include "RenderManager.h"
#include "Sprite.h"
#include "Texture.h"
#include "Renderer.h"
#include <render/Font.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

namespace king
{

RenderManager::RenderManager() {


}



RenderManager::~RenderManager() {

}



void RenderManager::init(const std::string& windowTitle, const u32 w,const u32 h, const bool fullscreen)
{

	int img_flags = IMG_INIT_JPG|IMG_INIT_PNG;
	int initted=IMG_Init(img_flags);
	if((initted&img_flags) != img_flags) {
		KING_merror(std::string("IMG_Init: Failed to init required jpg and png support! : ") + IMG_GetError() );
	}

	if (TTF_Init() != 0){
		KING_merror(std::string("TTF_Init:") + TTF_GetError());
	}


	u32 winflags = SDL_WINDOW_SHOWN;
	if(fullscreen)
		winflags |= SDL_WINDOW_FULLSCREEN;

	m_sdlwindow.reset(SDL_CreateWindow(windowTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, winflags));
	if (!m_sdlwindow) {
		KING_merror(std::string("SDL: Failed to create a valid SDL display window. SDLERROR=") + SDL_GetError());
	}

	m_renderer = king::make_unique<Renderer>(m_sdlwindow.get());

}

void RenderManager::registerRenderable(	const std::weak_ptr<Renderable>& renderable)
{
	m_renderables.push_back(renderable);
}

void RenderManager::render()
{

	m_renderables.remove_if([](std::weak_ptr<Renderable> p) {if (auto sptr = p.lock())return false; else return true;  } );

	for (auto rdrable : m_renderables)
	{
	    if (auto sptrenderable = rdrable.lock())
	    {
	    	if (sptrenderable->isVisible())
	    		sptrenderable->draw(m_renderer.get());

	    }
	}

	m_renderer->render();
}

std::shared_ptr<Texture> RenderManager::createTexture(const std::string& filename)
{
	auto itfound = m_textures.find(filename);
	if (itfound == m_textures.end())
	{
		auto ptr = std::make_shared<Texture>(m_renderer->getSDLRenderer());
		m_textures[filename]=ptr;
		return ptr;
	}
	else
	{
		return itfound->second;
	}
}

std::shared_ptr<Texture> RenderManager::createTexture()
{
	return std::make_shared<Texture>(m_renderer->getSDLRenderer());
}

std::shared_ptr<Font> RenderManager::createFont(const std::string& filename, const u32 size)
{

	auto itfound = m_fontsloaded.find(filename);

	if (itfound != m_fontsloaded.end()) // He trobat el nom
	{
		auto itsize = itfound->second.find(size);
		if (itsize != itfound->second.end()) //He trobat el size
			return itsize->second;
	}

	auto ptr = std::make_shared<Font>(m_renderer->getSDLRenderer(),size, filename);
	m_fontsloaded[filename][size]=ptr;
	return ptr;
}

}

