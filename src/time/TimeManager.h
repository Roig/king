

#ifndef TIME_TIMEMANAGER_H_
#define TIME_TIMEMANAGER_H_

#include <core/Types.h>

namespace king {

class TimeManager {
public:
	TimeManager();
	void update();
	f64 getDT() const;
	f64 getFPS() const;
	~TimeManager();

private:
	u64 m_lastTicks;
	f64 m_elapsedSeconds;
	f64 m_framesPerSecond;

	void calculateFPS();
	void calculcateDT();

};

} /* namespace king */

#endif /* TIME_TIMEMANAGER_H_ */
