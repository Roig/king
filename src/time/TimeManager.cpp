#include <time/TimeManager.h>
#include <SDL2/SDL.h>
namespace king {

static const f64 FPS_CAP_MAX = 80.0;
static const f64 DT_CAP_MAX = 0.5;
static const f64 MIN_DT = (1.0 / FPS_CAP_MAX);    //MAX FRAME CAP

TimeManager::TimeManager():
	m_lastTicks(0),m_elapsedSeconds(0),m_framesPerSecond(0)
{
}

TimeManager::~TimeManager() {
}



void TimeManager::update() {
	calculcateDT();
	calculateFPS();
}

f64 TimeManager::getDT() const {
	return m_elapsedSeconds;
}

f64 TimeManager::getFPS() const {
	return m_framesPerSecond;
}

void TimeManager::calculateFPS() {
	static u64 	FPScount = 0;		// Frame counter
	static f64	ETAccumSeconds = 0; // Elapsed time seconds accumulation per frame

	++FPScount;
	ETAccumSeconds += getDT();

	if(	ETAccumSeconds > 1.0 ) //Every second
	{
		m_framesPerSecond = FPScount/ETAccumSeconds;
		ETAccumSeconds = ETAccumSeconds - 1.0;
		FPScount = 0;
	}
}

void TimeManager::calculcateDT()
{
	if (!m_lastTicks)
		m_lastTicks = SDL_GetPerformanceCounter();

	f64 l_currentDT = 0;
	u64 l_currentticks;
	while (l_currentDT < MIN_DT)
	{
		l_currentticks = SDL_GetPerformanceCounter();
		l_currentDT = static_cast<f64>((l_currentticks - m_lastTicks)) / static_cast<f64>(SDL_GetPerformanceFrequency());
	}

	//For debugging purposes
	if (l_currentDT > DT_CAP_MAX)
		l_currentDT = MIN_DT;


	m_elapsedSeconds = l_currentDT;
	m_lastTicks = l_currentticks;
}

} /* namespace king */
